from typing import Callable
from typing import Optional
from typing import Union

import numpy as np
from numpy.typing import NDArray

from .element import Element
from .mesh import Mesh
from .mesh.block import ElementBlock
from .mesh.sets import ElementSet
from .mesh.sets import Nodeset
from .mesh.sets import Sideset


class Model:
    """A simple class representing a finite element model"""

    def __init__(self, name: str, *, mesh: Mesh) -> None:
        self.name = name
        self.mesh = mesh

        self.dirichlet_bcs: dict[str, float] = {}
        self.doftags = np.zeros((self.num_node, 7), dtype=int)
        self.dofvals = np.zeros((self.num_node, 7), dtype=float)
        self.dload_t = np.zeros(self.num_elem, dtype=int)
        self.dload_v: list[Union[None, Callable]] = [None] * self.num_elem
        self.sload_t = np.zeros((self.num_elem, 3), dtype=int)
        self.sload_v = np.zeros((self.num_elem, 3, 2), dtype=float)

        self.K: Optional[NDArray] = None
        self.F: Optional[NDArray] = None
        self.u: Optional[NDArray] = None
        self.R: Optional[NDArray] = None

    @property
    def num_node(self):
        return self.mesh.num_node

    @property
    def num_elem(self):
        return self.mesh.num_elem

    @property
    def coords(self):
        return self.mesh.coords

    @property
    def conn(self):
        return self.mesh.conn

    @property
    def num_dof(self):
        return self.num_node * self.dofs_per_node

    @property
    def sidesets(self) -> dict[str, Sideset]:
        return self.mesh.sidesets

    @property
    def nodesets(self) -> dict[str, Nodeset]:
        return self.mesh.nodesets

    @property
    def element_sets(self) -> dict[str, ElementSet]:
        return self.mesh.element_sets

    @property
    def blocks(self) -> dict[str, ElementBlock]:
        return self.mesh.blocks

    @property
    def dofs_per_node(self):
        for block in self.blocks.values():
            return block.dofs_per_node

    @property
    def force(self) -> NDArray:
        if self.F is None:
            raise ValueError("Model has not yet been solved")
        return self.F

    @property
    def stiffness(self) -> NDArray:
        if self.K is None:
            raise ValueError("Model has not yet been solved")
        return self.K

    @property
    def reaction(self) -> NDArray:
        if self.R is None:
            raise ValueError("Model has not yet been solved")
        return self.R

    @property
    def dofs(self) -> NDArray:
        if self.u is None:
            raise ValueError("Model has not yet been solved")
        return self.u

    def element_block(
        self, name: str, *, element: Element, elements: Union[str, list[int], NDArray]
    ) -> None:
        if tuple(element.node_freedom_table) != (0, 0, 0, 0, 0, 0, 1):
            raise TypeError("expected element with only temperature degree of freedom")
        self.mesh.element_block(name, element=element, elements=elements)

    def element_set(
        self, name: str, *, elements: Union[str, list[int], NDArray]
    ) -> None:
        self.mesh.element_set(name=name, elements=elements)

    def nodeset(self, name: str, *, nodes: Union[str, list[int], NDArray]) -> None:
        self.mesh.nodeset(name=name, nodes=nodes)

    def sideset(
        self, name: str, *, sides: Union[str, list[list[int]], NDArray]
    ) -> None:
        self.mesh.sideset(name=name, sides=sides)

    def temperature(self, nodeset: Union[str, NDArray], *, amplitude: float) -> None:
        if isinstance(nodeset, str):
            if nodeset not in self.nodesets:
                raise ValueError(f"{nodeset} is not a valid node set")
        else:
            # create an internal nodeset
            i, fmt = 0, "_Nodeset-{0}"
            while True:
                name = fmt.format(i)
                if name not in self.nodesets:
                    self.nodesets[name] = Nodeset(name=name, nodes=nodeset)
                    nodeset = name
                    break
                i += 1
        if nodeset in self.dirichlet_bcs:
            raise ValueError(f"{nodeset} already has prescribed temperature")
        self.dirichlet_bcs[nodeset] = amplitude

    def heat_source(self, elset: Union[str, NDArray], *, source=Callable) -> None:
        if isinstance(elset, str):
            if elset not in self.element_sets:
                raise ValueError(f"{elset} is not a valid element set")
        else:
            # create an internal element
            i, fmt = 0, "_Elset-{0}"
            while True:
                name = fmt.format(i)
                if name not in self.element_sets:
                    self.element_sets[name] = ElementSet(name=name, elements=elset)
                    elset = name
                    break
                i += 1
        for iid in self.element_sets[elset]:
            self.dload_t[iid] = 1
            self.dload_v[iid] = source

    def surface_flux(self, sideset: str, *, q: list[float]) -> None:
        if sideset not in self.sidesets:
            raise ValueError(f"{sideset} is not a valid side set")
        ss = self.sidesets[sideset]
        for iel, side in ss:
            self.sload_t[iel, side] = 1
            self.sload_v[iel, side] = q[:2]

    def surface_film(self, sideset: str, *, h: float, Too: float) -> None:
        if sideset not in self.sidesets:
            raise ValueError(f"{sideset} is not a valid side set")
        ss = self.sidesets[sideset]
        for iel, side in ss:
            self.sload_t[iel, side] = 2
            self.sload_v[iel, side] = [h, Too]

    def assemble_stiffness(self) -> NDArray:
        """Assemble the global stiffness matrix for a single dof/node problem

        Returns
        -------
        stiff : ndarray
            Global stiffness matrix stored as a full (nd*n, nd*n) symmetric matrix,
            where nd is the number of degrees of freedom per node and n the total
            number of nodes.

        """
        K = np.zeros((self.num_dof, self.num_dof), dtype=float)
        for block in self.blocks.values():
            ndof = block.dofs_per_node
            for iel in block.elements:
                nodes = self.mesh.conn[iel]
                dofs = [n * ndof + k for n in nodes for k in range(ndof)]
                xc = self.mesh.coords[dofs]
                ke = block.element.stiffness(xc, self.sload_t[iel], self.sload_v[iel])
                K[np.ix_(dofs, dofs)] += ke
        return K

    def assemble_force(self) -> NDArray:
        F = np.zeros(self.num_dof, dtype=float)
        for block in self.blocks.values():
            ndof = block.dofs_per_node
            for iel in block.elements:
                nodes = self.mesh.conn[iel]
                dofs = [n * ndof + k for n in nodes for k in range(ndof)]
                xc = self.mesh.coords[dofs]
                fe = block.element.force(
                    xc,
                    self.dload_t[iel],
                    self.dload_v[iel],
                    self.sload_t[iel],
                    self.sload_v[iel],
                )
                F[np.ix_(dofs)] += fe
        return F

    def apply_dirichlet_bcs(self, K: NDArray, F: NDArray) -> tuple[NDArray, NDArray]:
        """Apply Dirichlet (essential) boundary conditions to the global stiffness
        and global force.

        """
        Kbc = np.array(K)
        Fbc = np.array(F)
        ndof = self.dofs_per_node
        # extract only the active dofs for the heat transfer element
        doftags = self.doftags[:, [6]]
        dofvals = self.dofvals[:, [6]]
        for i, tags in enumerate(doftags):
            for j, tag in enumerate(tags):
                # i is the node number, j is the local dof
                if tag == 1:
                    dof = i * ndof + j
                    Kbc[dof, :] = 0
                    Kbc[dof, dof] = 1
                    Fbc[dof] = dofvals[dof]
        return Kbc, Fbc

    def create_dof_tables(self) -> None:
        for nodeset, amplitude in self.dirichlet_bcs.items():
            nodes = self.nodesets[nodeset].nodes
            self.doftags[nodes, 6] = 1
            self.dofvals[nodes, 6] = amplitude

    def solve(self) -> None:
        self.create_dof_tables()
        self.K = self.assemble_stiffness()
        self.F = self.assemble_force()
        Kbc, Fbc = self.apply_dirichlet_bcs(self.K, self.F)
        self.u = np.linalg.solve(Kbc, Fbc)
        self.R = np.dot(self.K, self.u) - self.F

    def triplot(self, show: bool = False, save: Optional[str] = None) -> None:
        import matplotlib.pyplot as plt
        import matplotlib.tri as mtri

        triang = mtri.Triangulation(
            self.mesh.coords[:, 0], self.mesh.coords[:, 1], self.mesh.conn
        )
        plt.tricontourf(triang, self.u, cmap="turbo", levels=60)
        plt.triplot(triang)
        # plt.quiver()
        plt.gca().set_aspect("equal")
        if show:
            plt.show()
        if save is not None:
            plt.savefig(save)
        return
