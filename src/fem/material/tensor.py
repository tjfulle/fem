import numpy as np
from numpy.typing import NDArray


def iso(A: NDArray) -> NDArray:
    """Returns the isotropic part of second order tensor A"""
    return trace(A) / 3.0 * identity(A.shape)


def dev(A: NDArray) -> NDArray:
    """Returns the deviatoric part of second order tensor A"""
    return A - iso(A)


def identity(shape: tuple) -> NDArray:
    if shape == (6,):
        return np.array([1.0, 1.0, 1.0, 0.0, 0.0, 0.0])
    elif shape == (9,):
        return np.reshape(np.eye(3), (9,))
    elif shape == (3, 3):
        return np.eye(3)
    else:
        raise ShapeError(shape)


def trace(A: NDArray) -> float:
    if A.shape == (6,):
        return sum(A[:3])
    elif A.shape == (9,):
        return sum(A[[0, 4, 8]])
    elif A.shape == (3, 3):
        return sum(A[[0, 1, 2], [0, 1, 2]])
    else:
        raise ShapeError(A.shape)


def transpose(A: NDArray) -> NDArray:
    if A.shape == (6,):
        return A
    elif A.shape == (9,):
        return A[[0, 3, 6, 1, 4, 7, 2, 5, 8]]
    elif A.shape == (3, 3):
        return A.T
    else:
        raise ShapeError(A.shape)


def sym(A: NDArray) -> NDArray:
    """Returns the symmetric part of second order tensor A"""
    if A.shape == (6,):
        return A
    elif A.shape == (9,):
        return 0.5 * (A + transpose(A))
    elif A.shape == (3, 3):
        return 0.5 * (A + A.T)
    else:
        raise ShapeError(A.shape)


def asarray(A: NDArray, symmetric: bool = False) -> NDArray:
    if A.shape != (3, 3):
        raise ShapeError(A.shape)
    if not symmetric:
        return A.flatten()
    A = 0.5 * (A.T + A)
    return np.array([A[0, 0], A[1, 1], A[2, 2], A[0, 1], A[0, 2], A[1, 2]])


def asmatrix(A: NDArray) -> NDArray:
    if A.shape == (6,):
        return np.array([[A[0], A[3], A[4]], [A[3], A[1], A[5]], [A[4], A[5], A[2]]])
    elif A.shape == (9,):
        return np.reshape(A, (3, 3))
    elif A.shape == (3, 3):
        return np.asarray(A)
    else:
        raise ShapeError(A.shape)


def norm(A: NDArray) -> float:
    if A.shape == (6,):
        return np.sqrt(np.sum(A * A))
    elif A.shape == (9,):
        return np.sqrt(np.sum(A * A))
    elif A.shape == (3, 3):
        return np.linalg.norm(A, ord="fro")
    else:
        raise ShapeError(A.shape)


def higham(F: NDArray, tol: float = 1e-14, maxit: int = 50) -> tuple[NDArray, NDArray]:
    """Higham iterative Polar Decomposition

    Parameters
    ----------
    F:
        deformation gradient tensor stored as a 3x3 matrix

    Returns
    -------
    R:
        proper orthogonal rotation tensor, stored as a 3x3 matrix
    U:
        symmetric positive-definite stretch tensor (stretch first) stored as a 6x1 array

    """
    I = np.eye(3)
    R = asmatrix(F).copy()
    i = 0
    while i < maxit:
        R = 0.5 * (np.linalg.inv(R.T) + R)
        E = 0.5 * (np.dot(R.T, R) - I)
        error = np.linalg.norm(E)
        if error < tol:
            break
        i += 1
    else:
        raise ValueError("Failed to find polar decomposition")
    U = np.dot(R.T, F)
    return R, U


class ShapeError(Exception):
    ...
