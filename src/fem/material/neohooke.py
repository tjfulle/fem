import numpy as np
from numpy.typing import NDArray

from .tensor import asarray


class NeoHooke:
    """Calculates the Neo Hookean stress"""

    def __init__(self, *, E: float, nu: float, rho: float = 1.0) -> None:
        self.lame_1 = E * nu / ((1.0 + nu) * (1.0 - 2.0 * nu))  # lambda
        self.lame_2 = E / (2.0 * (1 + nu))  # mu
        self.density = rho

    def eval(self, *, F: NDArray, **kwds) -> NDArray:
        """Calculate the stress associated with deformation gradient F

        Parameters
        ----------
        F:
            The deformation gradient represented as a 3x3 matrix

        Returns
        -------
        stress:
            The Cauchy stress represented as a 6x1 vectory

        """
        J = np.linalg.det(F)
        C = np.dot(F.T, F)
        Cinv = np.linalg.inv(C)
        I = np.eye(3)

        # Second Piola-Kirchhoff stress
        S = self.lame_1 * np.log(J) * Cinv + self.lame_2 * (I - Cinv)

        # Cauchy stress
        s = np.dot(np.dot(F, S), F.T) / J

        return asarray(s, symmetric=True)
