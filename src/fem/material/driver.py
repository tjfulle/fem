from typing import Optional

import matplotlib.pyplot as plt
import numpy as np
from numpy.typing import NDArray

from .base import Material
from .tensor import asarray
from .tensor import asmatrix
from .tensor import higham
from .tensor import sym


class MaterialDriver:
    # Number of state variables
    nsv = 40

    def __init__(self) -> None:
        self._material: Optional[Material] = None

        # Prescribed deformation
        # 0: time
        # 1-10: deformation gradient
        self.prdef = np.zeros((1, 10))

        # Initial deformation is the dentity
        self.prdef[0, 1:10] = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]

    @property
    def material(self) -> Material:
        return self._material

    @material.setter
    def material(self, arg: Material) -> None:
        self._material = arg

    def add_deformation_step(self, time: float, F: list) -> None:
        F = np.asarray(F)
        if F.size != 9:
            raise ValueError(f"Expected len(F) = 9, not {len(F)}")
        if time < self.prdef[-1, 0]:
            raise ValueError("time must increase monotonically")
        row = np.zeros(10)
        row[0] = time
        J = np.linalg.det(np.reshape(F, (3, 3)))
        if J <= 0:
            raise ValueError(f"det(F) = {J} must be > 0")
        row[1:10] = F.flatten()
        self.prdef = np.row_stack((self.prdef, row))

    def run(self, delta_time: float) -> None:
        if self.material is None:
            raise ValueError("assign material before running driver")
        tmin = self.prdef[0, 0]
        tmax = self.prdef[-1, 0]
        num = 1 + int((tmax - tmin) / delta_time)
        times = np.linspace(tmin, tmax, num)
        state = np.zeros((times.size, self.nsv))
        f_table = self.interp(times, self.prdef[:, 0], self.prdef[:, 1:])
        state[0, 0] = times[0]
        state[0, 1:10] = f_table[0]
        for i, row in enumerate(f_table[1:], start=1):
            T0 = state[i - 1, 13:19]
            F0 = np.reshape(f_table[i - 1], (3, 3))
            F = np.reshape(row, (3, 3))
            Fdot = (F - F0) / delta_time
            L = np.dot(Fdot, np.linalg.inv(F))
            D = asarray(sym(L), symmetric=True)
            E = asarray(0.5 * (np.dot(F.T, F) - np.eye(3)), symmetric=True)
            T = self.material.eval(F=F, D=D, T=T0, E=E, delta_time=delta_time)

            _, U = higham(F)
            I1 = np.trace(U)
            I2 = 0.5 * (np.trace(U) ** 2 - np.trace(np.dot(U, U)))
            I3 = np.linalg.det(U)

            state[i, 0] = times[i]
            state[i, 1:10] = row
            state[i, 10:13] = [I1, I2, I3]
            state[i, 13:19] = T
            state[i, 19:25] = self.pk2(F, T)
            state[i, 25:34] = self.pk1(F, T)
            state[i, 34:40] = E

        return state

    def pk2(self, F, s) -> NDArray:
        J = np.linalg.det(F)
        Fi = np.linalg.inv(F)
        T = asmatrix(s)
        S = J * np.dot(np.dot(Fi, T), Fi.T)
        return asarray(S, symmetric=True)

    def pk1(self, F, s) -> NDArray:
        J = np.linalg.det(F)
        Fi = np.linalg.inv(F)
        T = asmatrix(s)
        P = J * np.dot(Fi, T)
        return asarray(P)

    @staticmethod
    def plot(state: NDArray) -> None:
        plt.plot(state[:, 0], state[:, 10], label=r"$I_{1}$")
        plt.plot(state[:, 0], state[:, 11], label=r"$I_{2}$")
        plt.plot(state[:, 0], state[:, 12], label=r"$I_{3}$")
        plt.grid()
        plt.legend(loc="best")
        plt.show()

        plt.plot(state[:, 0], state[:, 13], label=r"$\sigma_{11}$")
        plt.plot(state[:, 0], state[:, 14], label=r"$\sigma_{22}$")
        plt.plot(state[:, 0], state[:, 15], label=r"$\sigma_{33}$")
        plt.plot(state[:, 0], state[:, 16], label=r"$\sigma_{12}$")
        plt.plot(state[:, 0], state[:, 17], label=r"$\sigma_{13}$")
        plt.plot(state[:, 0], state[:, 18], label=r"$\sigma_{23}$")
        plt.grid()
        plt.legend(loc="best")
        plt.show()

        plt.cla()
        plt.clf()
        plt.plot(state[:, 0], state[:, 19], label=r"$S_{11}$")
        plt.plot(state[:, 0], state[:, 20], label=r"$S_{22}$")
        plt.plot(state[:, 0], state[:, 21], label=r"$S_{33}$")
        plt.plot(state[:, 0], state[:, 22], label=r"$S_{12}$")
        plt.plot(state[:, 0], state[:, 23], label=r"$S_{13}$")
        plt.plot(state[:, 0], state[:, 24], label=r"$S_{23}$")
        plt.grid()
        plt.legend(loc="best")
        plt.show()

        plt.cla()
        plt.clf()
        plt.plot(state[:, 0], state[:, 25], label=r"$P_{11}$")
        plt.plot(state[:, 0], state[:, 26], label=r"$P_{12}$")
        plt.plot(state[:, 0], state[:, 27], label=r"$P_{13}$")
        plt.plot(state[:, 0], state[:, 28], label=r"$P_{21}$")
        plt.plot(state[:, 0], state[:, 29], label=r"$P_{22}$")
        plt.plot(state[:, 0], state[:, 30], label=r"$P_{23}$")
        plt.plot(state[:, 0], state[:, 31], label=r"$P_{31}$")
        plt.plot(state[:, 0], state[:, 32], label=r"$P_{32}$")
        plt.plot(state[:, 0], state[:, 33], label=r"$P_{33}$")
        plt.grid()
        plt.legend(loc="best")
        plt.show()

        plt.plot(state[:, 34], state[:, 13], label=r"$\sigma_{11}$")
        plt.plot(state[:, 34], state[:, 14], label=r"$\sigma_{22}$")
        plt.grid()
        plt.legend(loc="best")
        plt.show()

    @staticmethod
    def interp(x: NDArray, xp: NDArray, fp: NDArray) -> NDArray:
        """2D interpolation of a table"""
        interpolated = np.zeros((x.size, 9))
        for i in range(9):
            interpolated[:, i] = np.interp(x, xp, fp[:, i])
        return interpolated
