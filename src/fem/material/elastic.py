import numpy as np
from numpy.typing import NDArray

from .base import Material
from .tensor import asarray


class LinearElastic(Material):
    """Calculates the stress associated with small strain linear elasticity"""

    def __init__(self, *, E: float, nu: float, rho: float = 1.0) -> None:
        self.E = E
        self.nu = nu
        self.density = rho

    def stiffness(self, *, ndi: int = 3, nshr: int = 3) -> NDArray:
        if ndi == 3 and nshr == 3:
            C = np.zeros((6, 6))
            C11 = self.E * (1 - self.nu) / ((1 + self.nu) * (1 - 2 * self.nu))
            C12 = self.E * self.nu / ((1 + self.nu) * (1 - 2 * self.nu))
            C[0, :3] = [C11, C12, C12]
            C[1, :3] = [C12, C11, C12]
            C[2, :3] = [C12, C12, C11]
            C[3, 3] = C[4, 4] = C[5, 5] = (C11 - C12) / 2
        elif ndi == 3 and nshr == 1:
            # plane strain
            raise NotImplementedError
        elif ndi == 2 and nshr == 1:
            # plane stress
            E, nu = self.E, self.nu
            G = E / 2.0 / (1.0 + nu)
            Es = E / (1.0 - nu**2)
            return np.array([[Es, nu * Es, 0.0], [nu * Es, Es, 0.0], [0.0, 0.0, G]])
        else:
            raise NotImplementedError

    def eval(self, *, F: NDArray, **kwds) -> NDArray:
        """Calculate the stress associated with deformation gradient F

        Parameters
        ----------
        F:
            The deformation gradient represented as a 3x3 matrix

        Returns
        -------
        stress:
            The Cauchy stress represented as a 6x1 vector

        """
        # small strain tensor
        I = np.eye(3)
        e = asarray(0.5 * (F.T + F) - I, symmetric=True)
        # Convert to engineering strain
        e *= np.array([1, 1, 1, 2, 2, 2])
        C = self.stiffness()
        s = np.dot(C, e)
        return s
