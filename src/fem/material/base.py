from typing import Any

from numpy.typing import NDArray


class Material:
    def __init__(self, **params) -> None:
        for param, value in params.items():
            setattr(self, param, value)

    def eval(self, *args: Any, **kwargs: Any) -> NDArray:
        raise NotImplementedError
