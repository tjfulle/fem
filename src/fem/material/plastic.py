import numpy as np
from numpy.typing import NDArray

from .tensor import dev
from .tensor import iso
from .tensor import norm


class J2Plastic:
    """Calculates the stress associated with J2 plasticity"""

    def __init__(self, *, E: float, nu: float, k: float, rho: float = 1.0) -> None:
        self.K = E / 3.0 / (1.0 - 2.0 * nu)
        self.G = E / 2.0 / (1.0 + nu)
        self.k = k
        self.density = rho

    def eval(self, *, D, delta_time, T, **kwargs) -> NDArray:
        """Calculate the stress associated with deformation gradient F

        Parameters
        ----------
        D:
            The symmetric part of the velocity gradient
        T:
            The Cauchy stress at the beginning of the step
        delta_time:
            Time step

        Returns
        -------
        stress:
            The Cauchy stress represented as a 6x1 vectory

        """
        Ttrial = T + (3.0 * self.K * iso(D) + 2.0 * self.G * dev(D)) * delta_time
        fac = norm(dev(Ttrial)) / np.sqrt(2.0) / self.k
        if fac <= 1.0:
            # Elastic
            return Ttrial
        else:
            Tnew = iso(Ttrial) + dev(Ttrial) / fac
            # Tdot = (Tnew - T) / delta_time
            # De = iso(Tdot) / 3.0 / self.K + dev(Tdot) / 2.0 / self.G
            # Dp = D - De
            return Tnew
