from .abaparse import AbaqusModel


def read(filename):
    model = AbaqusModel(filename)
    nodtab = model.node_table()
    eletab = model.element_table()
    surfaces = model.surfaces.todict()
    return nodtab, eletab, model.nodesets, model.elsets, surfaces
