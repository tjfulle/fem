from typing import Callable
from typing import Optional

import numpy as np
from numpy.typing import NDArray

from ..material import Material
from .base import OFF
from .base import ON
from .base import Element


class C2D4S(Element):
    """4-node isoparametric plane stress element

    Notes
    -----
    - Node and element face numbering

                   [2]
                3-------2
                |       |
           [3]  |       | [1]
                |       |
                0-------1
                   [0]

    - Element is integrated by a 2x2 Gauss rule

    """

    dimension = 2
    num_node = 4
    node_freedom_table = [ON, ON, OFF, OFF, OFF, OFF, OFF]
    edges = np.array([[0, 1], [1, 2], [2, 3], [3, 0]])
    gauss_points = np.array(
        [[-1.0, -1.0], [1.0, -1.0], [1.0, 1.0], [-1.0, 1.0]]
    ) / np.sqrt(3.0)
    gauss_weights = np.ones(4)

    def __init__(self, *, material: Material, t: float = 1.0) -> None:
        self.material = material
        self.thickness = t

    def area(self, xc: NDArray) -> float:
        """Returns the area of the triangle

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        a : float
            The area

        """
        x1, x2, x3, x4 = xc[:, 0]
        y1, y2, y3, y4 = xc[:, 1]
        a = (x1 * y2 - x2 * y1) + (x2 * y3 - x3 * y2)
        a += (x3 * y4 - x4 * y3) + (x4 * y1 - x1 * y4)
        return a / 2.0

    def volume(self, xc):
        """Volume of the quadrilateral, `v = a(xc) * thickness"""
        return self.thickness * self.area(xc)

    def jacobian(self, xc: NDArray, xg: NDArray) -> float:
        """The Jacobian of the transformation from phyiscal to natural coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        J : float
            J = det(Jm), where Jm is the Jacobian matrix Jm = dx/ds

        """
        dNds = self.shape_grad(xg)
        dxds = np.dot(dNds, xc)
        return np.linalg.det(dxds)

    def shape(self, xc: NDArray, xg: NDArray) -> NDArray:
        """Shape functions in the triangle (natural) coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        s, t = xg
        N1 = (1.0 - s) * (1.0 - t)
        N2 = (1.0 + s) * (1.0 - t)
        N3 = (1.0 + s) * (1.0 + t)
        N4 = (1.0 - s) * (1.0 + t)
        N = np.array([N1, N2, N3, N4])
        return N / 4.0

    def shape_grad(self, xg: NDArray) -> NDArray:
        """Derivatives of shape functions, wrt to the physical coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        s, t = xg
        dN = np.array(
            [
                [-1.0 + t, 1.0 - t, 1.0 + t, -1.0 - t],
                [-1.0 + s, -1.0 - s, 1.0 + s, 1.0 - s],
            ]
        )
        return dN / 4.0

    def bmatrix(self, xc: NDArray, xg: NDArray) -> NDArray:
        """Compute the element B matrix which contains derivatives of shape
        functions, wrt to physical coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        B : ndarray
            The B matrix, or matrix containing derivatives of the shape function
            with respect to the physical coordinates

        Notes
        -----
        The shape functions are cast in terms of the natural coordinates.  Use
        the chain rule find their derivatives in the physical coordinates:

            dN/dx = dN/ds * ds/dx = dN/ds * inv(dx/ds)

        where

            dx/ds = d(sum(N * xi))/ds = sum(dN/ds * xi)

        """
        num_node = self.num_node
        dofs_per_node = self.dofs_per_node
        B = np.zeros((3, dofs_per_node * num_node))
        dNds = self.shape_grad(xg)
        dxds = np.dot(dNds, xc)
        dsdx = np.linalg.inv(dxds)
        dNdx = np.dot(dsdx, dNds)
        B[0, 0::dofs_per_node] = B[2, 1::dofs_per_node] = dNdx[0, :]
        B[1, 1::dofs_per_node] = B[2, 0::dofs_per_node] = dNdx[1, :]
        return B

    def pmatrix(self, xg: NDArray) -> NDArray:
        """Compute the element P matrix

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates
        xg : list of float
            s, t = p

        Returns
        -------
        P : ndarray
            The P matrix, or matrix containing the shape functions.

        Notes
        -----
        The P matrix is setup such that values of an element constant field `f`
        can be interpolated to a point as:

            x = dot(P.T, f)

        """
        num_node = self.num_node
        dofs_per_node = self.dofs_per_node
        N = self.shape(xg)
        P = np.zeros((dofs_per_node, num_node * dofs_per_node))
        P[0, 0::dofs_per_node] = N
        P[1, 1::dofs_per_node] = N
        return P

    def stiffness(self, xc: NDArray) -> NDArray:
        """Compute the element stiffness

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates

        Returns
        -------
        ke : ndarray
            The element stiffess
            ke = integrate(B.T, C, B)

        """
        ke = np.zeros((8, 8))
        p1 = p2 = 2
        for p in range(p1 * p2):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            Be = self.bmatrix(xc, xg)
            Je = self.jacobian(xc, xg)
            De = self.material.stiffness(ndi=2, nshr=1)
            ke += Je * wg * np.dot(np.dot(Be.T, De), Be)
        return ke

    def force(
        self,
        xc: NDArray,
        *,
        dload: Optional[Callable],
        dsload: Optional[dict[int, NDArray]],
    ) -> NDArray:
        """Compute the element force

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates

        Returns
        -------
        fe : ndarray
            The element force
            fe = integrate(transpose(P).f)

        """
        fe = np.zeros(8, dtype=float)
        if dload is not None:
            fe += self.body_force(xc, dload)
        if dsload is not None:
            for side, q in dsload.items():
                fe += self.surface_force(xc, side, q)
        return fe

    def body_force(self, xc: NDArray, dload: Callable) -> NDArray:
        """Compute the element force

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        dload : callable

        Returns
        -------
        fe : ndarray
            The element force
            fe = integrate(transpose(P).f)

        """
        fe = np.zeros(8, dtype=float)
        for p, xg in enumerate(self.gauss_points):
            wg = self.gauss_weights[p]
            Je = self.jacobian(xc, xg)
            Pe = self.pmatrix(xg)
            he = self.thickness
            Ne = self.shape(xc, xg)
            f = dload(np.dot(Ne, xc))
            fe += Je * wg * he * np.dot(Pe.T, f)
        return fe

    def surface_force(self, xc: NDArray, edge_no: int, q: NDArray) -> NDArray:
        """Calculate the nodal force contributions from a surface load `q`

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates
        edge : ndarray
            Internal node IDs of the surface
        q : ndarray
            The surface load

        """
        fe = np.zeros(8, dtype=float)
        edge = self.edges[edge_no]
        xd = xc[edge]

        # Gauss points/weights on 1D edge
        bgauss_points = np.array([-1.0, 1.0]) / np.sqrt(3.0)
        bgauss_weights = np.array([1.0, 1.0])
        bshape = lambda xg: np.array([1.0 - xg, 1.0 + xg]) / 2.0
        bgrad = lambda xg: np.array([-1.0, 1.0]) / 2.0

        dofs = [
            ni * self.dofs_per_node + j
            for ni in edge
            for j in range(self.dofs_per_node)
        ]

        for p, xg in enumerate(bgauss_points):
            wg = bgauss_weights[p]
            N = bshape(xg)
            dNds = bgrad(xg)
            dxds = np.dot(dNds, xd)
            Jd = np.sqrt(dxds[0] ** 2 + dxds[1] ** 2)
            Pe = np.zeros((2, 4))
            Pe[0, 0::2] = N
            Pe[1, 1::2] = N
            fe[dofs] += wg * Jd * np.dot(Pe.T, q)
        return fe
