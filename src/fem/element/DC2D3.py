from typing import Callable
from typing import Optional

import numpy as np
from numpy.typing import NDArray

from .. import geom
from .base import OFF
from .base import ON
from .base import Element


class DC2D3(Element):
    """3 Node triangular element"""

    dimension = 2
    num_node = 3
    node_freedom_table = [OFF, OFF, OFF, OFF, OFF, OFF, ON]
    edges = np.array([[0, 1], [1, 2], [2, 0]])
    gauss_points = np.array([[1.0, 1.0], [4.0, 1.0], [1.0, 4.0]]) / 6.0
    gauss_weights = np.array([1.0, 1.0, 1.0]) / 6.0

    def __init__(self, *, k: float, t: float = 1.0) -> None:
        self.k = k
        self.thickness = t

    def area(self, xc: NDArray) -> float:
        """Returns the area of the triangle

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        a : float
            The area

        """
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        a = (x1 * y2 - x2 * y1) - (x1 * y3 - x3 * y1) + (x2 * y3 - x3 * y2)
        return a / 2.0

    def volume(self, xc: NDArray) -> float:
        """Volume of the triangle, `v = a(xc) * thickness"""
        return self.area(xc) * self.thickness

    def jacobian(self, xc: NDArray) -> float:
        """The Jacobian of the transformation from phyiscal to natural coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        J : float
            J = det(Jm), where Jm is the Jacobian matrix Jm = dx/ds

        """
        return 2 * self.area(xc)

    def isoparametric_map(self, xc: NDArray, xg: NDArray) -> tuple[float, float]:
        """Map the point `xg` in natural coordinates to physical coordinates

        Paratemeters
        ------------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            Point in natural coordinates
            s, t = p

        """
        s, t = xg
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        x = x1 * s + x2 * t + x3 * (1 - s - t)
        y = y1 * s + y2 * t + y3 * (1 - s - t)
        return x, y

    def shape(self, xc: NDArray, xg: NDArray) -> NDArray:
        """Shape functions in the triangle (natural) coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates
        xg : ndarray
            Gauss point coordinate

        Returns
        -------
        N : ndarray
            N[i] is the ith shape function evaluated at p

        """
        x, y = self.isoparametric_map(xc, xg)
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        N1 = x * (y2 - y3) + y * (x3 - x2) + x2 * y3 - x3 * y2
        N2 = x * (y3 - y1) + y * (x1 - x3) - x1 * y3 + x3 * y1
        N3 = x * (y1 - y2) + y * (x2 - x1) + x1 * y2 - x2 * y1
        N = np.array([N1, N2, N3]) / (2.0 * self.area(xc))
        return N

    def shape_grad(self, xc: NDArray) -> NDArray:
        """Derivatives of shape functions, wrt to triangle (natural) coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        dN : ndarray
            dN[i] is the derivative of ith shape function

        """
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        dN = np.array([[y2 - y3, y3 - y1, y1 - y2], [x3 - x2, x1 - x3, x2 - x1]])
        return dN / (2.0 * self.area(xc))

    def bmatrix(self, xc: NDArray) -> NDArray:
        """Compute the element B matrix which contains derivatives of shape
        functions, wrt to physical coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        B : ndarray
            The B matrix, or matrix containing derivatives of the shape function
            with respect to the physical coordinates

        Notes
        -----
        The shape functions are cast in terms of the natural coordinates.  Use
        the chain rule find their derivatives in the physical coordinates:

            dN/dx = dN/ds * ds/dx = dN/ds * inv(dx/ds)

        where

            dx/ds = d(sum(N * xi))/ds = sum(dN/ds * xi)

        """
        return self.shape_grad(xc)

    def pmatrix(self, xc, p):
        """Compute the element P matrix

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        P : ndarray
            The S matrix, or matrix containing the shape functions.

        Notes
        -----
        The P matrix is setup such that values of an element constant field `f`
        can be interpolated to a point as:

            x = dot(P.T, f)

        """
        return self.shape(xc, p)

    def stiffness(self, xc: NDArray, tags: NDArray, vals: NDArray) -> NDArray:
        ke = self.stiffness1(xc)
        for side, tag in enumerate(tags):
            if tag == 2:
                h, Too = vals[side]
                ke += self.stiffness2(xc, side, Too, h)
        return ke

    def stiffness1(self, xc: NDArray) -> NDArray:
        """Compute the stiffness"""
        ke = np.zeros((3, 3))
        D = np.array([[self.k, 0.0], [0.0, self.k]])
        for p in range(len(self.gauss_weights)):
            Be = self.bmatrix(xc)
            Je = self.jacobian(xc)
            wg = self.gauss_weights[p]
            ke += Je * wg * np.dot(np.dot(Be.T, D), Be)
        return ke

    def stiffness2(self, xc: NDArray, edge_no: int, Too: float, h: float) -> NDArray:
        ke = np.zeros((3, 3))
        edge = self.edges[edge_no]
        xd = xc[edge]
        dx, dy = xd[1, 0] - xd[0, 0], xd[1, 1] - xd[0, 1]
        hd = np.sqrt(dx**2 + dy**2)
        for p in range(2):
            # gauss integration along the edge
            xg = [-1.0 / np.sqrt(3.0), 1.0 / np.sqrt(3.0)][p]
            wg = [1.0, 1.0][p]
            N = np.array([(1.0 - xg) / 2.0, (1.0 + xg) / 2.0])
            ix = np.ix_(edge, edge)
            ke[ix] += h * hd / 2 * wg * np.outer(N, N)
        return ke

    def force(
        self,
        xc: NDArray,
        dload_t: int,
        dload_v: Optional[Callable],
        sload_t: NDArray,
        sload_v: NDArray,
    ) -> NDArray:
        """Calculate the nodal force contribution from distributed and surface loads

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        """
        fe = np.zeros(3)
        if dload_t == 1:
            assert dload_v is not None
            fe += self.source_array(xc, dload_v)
        for side, tag in enumerate(sload_t):
            if tag == 1:
                fe += self.conduction_flux_array(xc, side, sload_v[side])
            elif tag == 2:
                fe += self.convection_flux_array(xc, side, *sload_v[side])
        return fe

    def source_array(self, xc: NDArray, f: Callable) -> NDArray:
        fe = np.zeros(3, dtype=float)
        for p in range(len(self.gauss_points)):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            Je = self.jacobian(xc)
            Ne = self.shape(xc, xg)
            fe += Je * wg * Ne * f(np.dot(Ne, xc))
        return fe

    def convection_flux_array(self, xc: NDArray, edge_no: int, h: float, Too: float):
        fe = self.boundary_flux_array_n(xc, edge_no, Too * h)
        return fe

    def conduction_flux_array(self, xc: NDArray, edge_no: int, q: NDArray) -> NDArray:
        edge = self.edges[edge_no]
        xd = xc[edge]
        n = geom.edge_normal(xd)
        fac = 1.0 if geom.cost(q[:2], n) > 0 else -1.0
        qn = fac * np.dot(q[:2], n)
        fe = self.boundary_flux_array_n(xc, edge_no, qn)
        return fe

    def boundary_flux_array_n(self, xc: NDArray, edge_no: int, qn: NDArray) -> NDArray:
        fe = np.zeros(3, dtype=float)
        edge = self.edges[edge_no]
        xd = xc[edge]
        dx, dy = xd[1, 0] - xd[0, 0], xd[1, 1] - xd[0, 1]
        hd = np.sqrt(dx**2 + dy**2)
        for p in range(2):
            # gauss integration along the edge
            xg = [-1.0 / np.sqrt(3.0), 1.0 / np.sqrt(3.0)][p]
            wg = [1.0, 1.0][p]
            N = np.array([(1.0 - xg) / 2.0, (1.0 + xg) / 2.0])
            fe[edge] += hd / 2 * wg * N * qn
        return fe
