from numpy.typing import NDArray

from fem.lang import classproperty

OFF, ON = 0, 1


class Element:
    num_node: int
    dimension: int

    # The node freedom table describes the element's active
    # freedoms.  It is a list of length 7 with the following meaning:
    #    signature[0] is ON if DOF0 (x displacement) is active
    #    signature[1] is ON if DOF1 (y displacement) is active
    #    signature[2] is ON if DOF2 (z displacement) is active
    #    signature[3] is ON if DOF3 (rotation about the x axis) is active
    #    signature[4] is ON if DOF4 (rotation about the y axis) is active
    #    signature[5] is ON if DOF5 (rotation about the z axis) is active
    #    signature[6] is ON if DOF6 (temperature) is active
    #    signature[7] is ON if DOF7 is active (this is a placeholder)
    node_freedom_table: list[int]

    #: ndir is the number of direct components in the stress tensor. The direct
    # components are the XX, YY, and ZZ components of the stress.
    ndir: int

    #: nshr is the number of shear components in the stress tensor.
    nshr: int

    #: num_gauss is the number of integration points.
    num_gauss: int

    #: cp defines the center of the element *in the natural coordinates*
    cp: NDArray

    #: xp defines the corner nodes *in the natural coordinates*
    xp: NDArray

    #: edges defines the nodes that define the edges of the element
    edges: int

    def stiffness(self, *args, **kwargs) -> NDArray:
        raise NotImplementedError

    def force(self, *args, **kwargs) -> NDArray:
        raise NotImplementedError

    def shape(self, *args, **kwargs) -> NDArray:
        raise NotImplementedError

    def grad(self, *args, **kwargs) -> NDArray:
        raise NotImplementedError

    @classproperty
    def dofs_per_node(self):
        return sum(self.node_freedom_table)

    @classproperty
    def active_dofs(self):
        return [i for (i, dof) in enumerate(self.node_freedom_table) if dof]
