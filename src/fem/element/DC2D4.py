from typing import Callable
from typing import Optional

import numpy as np
from numpy.typing import NDArray

from .. import geom
from .base import Element


class DC2D4(Element):
    """4-node fully integrated isoparametric plane diffusive heat transfer element

    Notes
    -----
    - Node and element face numbering

                   [2]
                3-------2
                |       |
           [3]  |       | [1]
                |       |
                0-------1
                   [0]

    - Element is integrated by a 2x2 Gauss rule

    """

    dimension = 2
    num_node = 4
    node_freedom_table = [0, 0, 0, 0, 0, 0, 1]
    edges = np.array([[0, 1], [1, 2], [2, 3], [3, 0]])
    gauss_points = np.array(
        [[-1.0, -1.0], [1.0, -1.0], [1.0, 1.0], [-1.0, 1.0]]
    ) / np.sqrt(3.0)
    gauss_weights = np.ones(4)

    def __init__(self, *, k: float, t: float = 1.0) -> None:
        self.k = k
        self.thickness = t

    def area(self, xc: NDArray) -> float:
        """Returns the area of the triangle

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        a : float
            The area

        """
        x1, x2, x3, x4 = xc[:, 0]
        y1, y2, y3, y4 = xc[:, 1]
        a = (x1 * y2 - x2 * y1) + (x2 * y3 - x3 * y2)
        a += (x3 * y4 - x4 * y3) + (x4 * y1 - x1 * y4)
        return a / 2.0

    def volume(self, xc: NDArray) -> float:
        """Volume of the quadrilateral, `v = a(xc) * thickness"""
        return self.thickness * self.area(xc)

    def jacobian(self, xc: NDArray, xg: NDArray) -> float:
        """The Jacobian of the transformation from phyiscal to natural coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates

        Returns
        -------
        J : float
            J = det(Jm), where Jm is the Jacobian matrix Jm = dx/ds

        """
        dNds = self.shape_grad(xg)
        dxds = np.dot(dNds, xc)
        return np.linalg.det(dxds)

    def shape(self, xg: NDArray) -> NDArray:
        """Shape functions in the natural coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        s, t = xg
        N1 = (1.0 - s) * (1.0 - t)
        N2 = (1.0 + s) * (1.0 - t)
        N3 = (1.0 + s) * (1.0 + t)
        N4 = (1.0 - s) * (1.0 + t)
        N = np.array([N1, N2, N3, N4]) / 4.0
        return N

    def shape_grad(self, xg: NDArray) -> NDArray:
        """Derivatives of shape functions, wrt to natural coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        s, t = xg
        dNds = [-1.0 + t, 1.0 - t, 1.0 + t, -1.0 - t]
        dNdt = [-1.0 + s, -1.0 - s, 1.0 + s, 1.0 - s]
        dN = np.array([dNds, dNdt]) / 4.0
        return dN

    def stiffness(self, xc: NDArray, tags: NDArray, vals: NDArray) -> NDArray:
        ke = self.stiffness1(xc)
        for side, tag in enumerate(tags):
            if tag == 2:
                h, Too = vals[side]
                ke += self.stiffness2(xc, side, Too, h)
        return ke

    def stiffness1(self, xc: NDArray) -> NDArray:
        ke = np.zeros((4, 4))
        D = np.array([[self.k, 0.0], [0.0, self.k]])
        for p in range(len(self.gauss_weights)):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            Be = self.bmatrix(xc, xg)
            Je = self.jacobian(xc, xg)
            ke += Je * wg * np.dot(np.dot(Be.T, D), Be)
        return ke

    def stiffness2(self, xc: NDArray, edge_no: int, Too: float, h: float) -> NDArray:
        ke = np.zeros((4, 4))
        edge = self.edges[edge_no]
        xd = xc[edge]
        dx, dy = xd[1, 0] - xd[0, 0], xd[1, 1] - xd[0, 1]
        hd = np.sqrt(dx**2 + dy**2)
        for p in range(2):
            xg = [-1.0 / np.sqrt(3), 1.0 / np.sqrt(3)][p]
            wg = 1.0
            N = np.array([(1.0 - xg) / 2.0, (1.0 + xg) / 2.0])
            ix = np.ix_(edge, edge)
            ke[ix] += h * hd / 2 * wg * np.outer(N, N)
        return ke

    def bmatrix(self, xc: NDArray, xg: NDArray) -> NDArray:
        """Compute the element B matrix which contains derivatives of shape
        functions, wrt to physical coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        B : ndarray
            The B matrix, or matrix containing derivatives of the shape function
            with respect to the physical coordinates

        Notes
        -----
        The shape functions are cast in terms of the natural coordinates.  Use
        the chain rule find their derivatives in the physical coordinates:

            dN/dx = dN/ds * ds/dx = dN/ds * inv(dx/ds)

        where

            dx/ds = d(sum(N * xi))/ds = sum(dN/ds * xi)

        """
        dNds = self.shape_grad(xg)
        dxds = np.dot(dNds, xc)
        dsdx = np.linalg.inv(dxds)
        dNdx = np.dot(dsdx, dNds)
        return dNdx

    def pmatrix(self, xg: NDArray) -> NDArray:
        """Compute the element P matrix

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        P : ndarray
            The S matrix, or matrix containing the shape functions.

        Notes
        -----
        The P matrix is setup such that values of an element constant field `f`
        can be interpolated to a point as:

            x = dot(P.T, f)

        """
        return self.shape(xg)

    def force(
        self,
        xc: NDArray,
        dload_t: int,
        dload_v: Optional[Callable],
        sload_t: NDArray,
        sload_v: NDArray,
    ) -> NDArray:
        """Calculate the nodal force contribution from distributed and surface loads

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates

        """
        fe = np.zeros(4)
        if dload_t == 1:
            assert dload_v is not None
            fe += self.source_array(xc, dload_v)
        for side, tag in enumerate(sload_t):
            if tag == 1:
                fe += self.conduction_flux_array(xc, side, sload_v[side])
            elif tag == 2:
                fe += self.convection_flux_array(xc, side, *sload_v[side])
        return fe

    def source_array(self, xc: NDArray, f: Callable) -> NDArray:
        fe = np.zeros(4)
        for p in range(len(self.gauss_points)):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            Je = self.jacobian(xc, xg)
            Ne = self.shape(xg)
            fe += Je * wg * Ne * f(np.dot(Ne, xc))
        return fe

    def convection_flux_array(self, xc: NDArray, edge_no: int, h: float, Too: float):
        fe = self.boundary_flux_array_n(xc, edge_no, Too * h)
        return fe

    def conduction_flux_array(self, xc: NDArray, edge_no: int, q: NDArray) -> NDArray:
        edge = self.edges[edge_no]
        xd = xc[edge]
        n = geom.edge_normal(xd)
        fac = 1 if geom.cost(q[:2], n) > 0 else -1
        qn = fac * np.dot(q[:2], n)
        fe = self.boundary_flux_array_n(xc, edge_no, qn)
        return fe

    def boundary_flux_array_n(self, xc: NDArray, edge_no: int, qn: NDArray) -> NDArray:
        fe = np.zeros(4)
        edge = self.edges[edge_no]
        xd = xc[edge]
        for p in range(len(self.edge.gauss_points)):
            xg = [-1.0 / np.sqrt(3), 1.0 / np.sqrt(3)][p]
            wg = 1.0
            N = np.array([(1.0 - xg) / 2.0, (1.0 + xg) / 2.0])
            dNds = np.array([-0.5, 0.5])
            dxds = np.dot(dNds, xd)
            Jd = np.sqrt(dxds[0] ** 2 + dxds[1] ** 2)
            fe[edge] += wg * Jd * N * qn
        return fe
