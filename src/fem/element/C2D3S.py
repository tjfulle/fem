from typing import Callable
from typing import Optional

import numpy as np
from numpy.typing import NDArray

from ..material import Material
from .base import OFF
from .base import ON
from .base import Element

# TODO:
#  1. modify element node_freedom_table to have the correct active dofs
#  2. re-dimension stiffness and forces
#  3. modify element to handle two dofs per node:
#     - b matrix
#  4. modify how forces are scattered into the element force array
#  5. take Young's modulus and Poisson's ratio as inputs and store them


class C2D3S(Element):
    """3 Node triangular element"""

    dimension = 2
    num_node = 3
    node_freedom_table = [ON, ON, OFF, OFF, OFF, OFF, OFF]
    edges = np.array([[0, 1], [1, 2], [2, 0]])
    gauss_points = np.array([[1.0, 1.0], [4.0, 1.0], [1.0, 4.0]]) / 6.0
    gauss_weights = np.array([1.0, 1.0, 1.0]) / 6.0

    def __init__(self, *, material: Material, t: float = 1.0) -> None:
        self.material = material
        self.thickness = t

    def area(self, xc: NDArray) -> float:
        """Returns the area of the triangle

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        a : float
            The area

        """
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        a = (x1 * y2 - x2 * y1) - (x1 * y3 - x3 * y1) + (x2 * y3 - x3 * y2)
        return a / 2.0

    def volume(self, xc):
        """Volume of the quadrilateral, `v = a(xc) * thickness"""
        return self.thickness * self.area(xc)

    def jacobian(self, xc: NDArray) -> float:
        """The Jacobian of the transformation from phyiscal to natural coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        J : float
            J = det(Jm), where Jm is the Jacobian matrix Jm = dx/ds

        """
        return 2 * self.area(xc)

    def shape(self, xc: NDArray, xg: NDArray) -> NDArray:
        """Shape functions in the triangle (natural) coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        x, y = self.isoparametric_map(xc, xg)
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        N1 = x * (y2 - y3) + y * (x3 - x2) + x2 * y3 - x3 * y2
        N2 = x * (y3 - y1) + y * (x1 - x3) - x1 * y3 + x3 * y1
        N3 = x * (y1 - y2) + y * (x2 - x1) + x1 * y2 - x2 * y1
        N = np.array([N1, N2, N3]) / (2.0 * self.area(xc))
        return N

    def shape_grad(self, xc: NDArray) -> NDArray:
        """Derivatives of shape functions, wrt to the physical coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        dN = np.array([[y2 - y3, y3 - y1, y1 - y2], [x3 - x2, x1 - x3, x2 - x1]])
        return dN / 2.0 / self.area(xc)

    def isoparametric_map(self, xc: NDArray, xg: NDArray) -> tuple[float, float]:
        """Map the point `xg` in natural coordinates to physical coordinates

        Paratemeters
        ------------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            Point in natural coordinates
            s, t = p

        """
        s, t = xg
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        x = x1 * s + x2 * t + x3 * (1 - s - t)
        y = y1 * s + y2 * t + y3 * (1 - s - t)
        return x, y

    def bmatrix(self, xc: NDArray, xg: Optional[NDArray] = None) -> NDArray:
        """Compute the element B matrix

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        B : ndarray
            The B matrix, or matrix containing derivatives of the shape function
            with respect to the physical coordinates

        """
        dNdx = self.shape_grad(xc)
        num_node = self.num_node
        dofs_per_node = self.dofs_per_node
        B = np.zeros((3, dofs_per_node * num_node))
        B[0, 0::dofs_per_node] = B[2, 1::dofs_per_node] = dNdx[0, :]
        B[1, 1::dofs_per_node] = B[2, 0::dofs_per_node] = dNdx[1, :]
        return B

    def pmatrix(self, xc: NDArray, xg: NDArray) -> NDArray:
        """Compute the element P matrix

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates
        xg : list of float
            s, t = p

        Returns
        -------
        P : ndarray
            The P matrix, or matrix containing the shape functions.

        Notes
        -----
        The P matrix is setup such that values of an element constant field `f`
        can be interpolated to a point as:

            x = dot(P.T, f)

        """
        num_node = self.num_node
        dofs_per_node = self.dofs_per_node
        N = self.shape(xc, xg)
        P = np.zeros((dofs_per_node, num_node * dofs_per_node))
        P[0, 0::dofs_per_node] = N
        P[1, 1::dofs_per_node] = N
        return P

    def stiffness(self, xc: NDArray) -> NDArray:
        """Compute the element stiffness

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates

        Returns
        -------
        ke : ndarray
            The element stiffess
            ke = integrate(B.T, C, B)

        """
        Be = self.bmatrix(xc)
        Je = self.jacobian(xc)
        he = self.thickness
        D = self.material.stiffness(ndi=2, nshr=1)
        ke = np.zeros((6, 6))
        for p in range(len(self.gauss_weights)):
            wg = self.gauss_weights[p]
            ke += Je * wg * he * np.dot(np.dot(Be.T, D), Be)
        return ke

    def force(
        self,
        xc: NDArray,
        *,
        dload: Optional[Callable],
        dsload: Optional[dict[int, NDArray]],
    ) -> NDArray:
        """Calculate the nodal force contribution from distributed and surface loads"""
        # TODO:
        # redimension force and make sure to put force values into the right
        # element force components
        fe = np.zeros(6, dtype=float)
        if dload is not None:
            fe += self.body_force(xc, dload)
        if dsload is not None:
            for side, q in dsload.items():
                fe += self.surface_force(xc, side, q)
        return fe

    def body_force(self, xc: NDArray, dload: Callable) -> NDArray:
        """Compute the element force

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        dload : callable

        Returns
        -------
        fe : ndarray
            The element force
            fe = integrate(transpose(P).f)

        """
        fe = np.zeros(6, dtype=float)
        he = self.thickness
        Je = self.jacobian(xc)
        for p, xg in enumerate(self.gauss_points):
            wg = self.gauss_weights[p]
            Ne = self.shape(xc, xg)
            Pe = self.pmatrix(xc, xg)
            f = dload(np.dot(Ne, xc))
            fe += Je * wg * he * np.dot(Pe.T, f)
        return fe

    def surface_force(self, xc: NDArray, edge_no: int, q: NDArray) -> NDArray:
        """Calculate the nodal force contributions from a surface load `q`

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates
        edge : ndarray
            Internal node IDs of the surface
        q : ndarray
            The surface load

        """
        fe = np.zeros(6, dtype=float)
        edge = self.edges[edge_no]
        xd = xc[edge]

        # Gauss points/weights on 1D edge
        bgauss_points = np.array([-1.0, 1.0]) / np.sqrt(3.0)
        bgauss_weights = np.array([1.0, 1.0])
        bshape = lambda xg: np.array([1.0 - xg, 1.0 + xg]) / 2.0
        bgrad = lambda xg: np.array([-1.0, 1.0]) / 2.0

        dofs = [
            ni * self.dofs_per_node + j
            for ni in edge
            for j in range(self.dofs_per_node)
        ]

        for p, xg in enumerate(bgauss_points):
            wg = bgauss_weights[p]
            N = bshape(xg)
            dNds = bgrad(xg)
            dxds = np.dot(dNds, xd)
            Jd = np.sqrt(dxds[0] ** 2 + dxds[1] ** 2)
            Pe = np.zeros((2, 4))
            Pe[0, 0::2] = N
            Pe[1, 1::2] = N
            fe[dofs] += wg * Jd * np.dot(Pe.T, q)
        return fe
