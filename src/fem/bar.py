import numpy as np
from numpy.typing import NDArray

num_dofs_per_node = 1
gauss_points = np.array([-1.0 / np.sqrt(3.0), 1.0 / np.sqrt(3.0)])
gauss_weights = np.array([1.0, 1.0])


def mesh(length: float, *, num_ele: int = 10) -> tuple[NDArray, NDArray]:
    """Mesh a 1 dimensional computation domain

    Parameters
    ----------
    length : float
        The length of the domain
    num_ele : int
        The number of elements in the mesh

    """
    conn = np.array([[n, n + 1] for n in range(num_ele)], dtype=int)
    coords = np.linspace(0.0, length, num_ele + 1)
    return coords, conn


def global_stiffness(coords: NDArray, conn: NDArray, A: NDArray, E: NDArray) -> NDArray:
    """Assemble the global stiffness matrix for a single dof/node problem

    Parameters
    ----------
    coords : ndarray
        Nodal coordinates.  coords[i] is the ith coordinate of node n
    conn : ndarray
        Table of element connectivity
        conn[i, j] is the jth node of the ith element
    A : ndarray
        A[e] is the cross-sectional area of the eth element
    E : ndarray
        E[e] is the Young's modulus of the eth element

    Returns
    -------
    stiff : ndarray
        Global stiffness matrix stored as a full (nd*n, nd*n) symmetric matrix,
        where nd is the number of degrees of freedom per node and n the total
        number of nodes.

    """
    num_nodes = len(np.unique(conn))
    num_dofs = num_nodes * num_dofs_per_node
    K = np.zeros((num_dofs, num_dofs), dtype=float)
    for e, dofs in enumerate(conn):
        xc = coords[dofs]
        ke = np.zeros((2, 2))
        for p in range(len(gauss_points)):
            xg = gauss_points[p]
            wg = gauss_weights[p]
            Je = jacobian(xc)
            Be = bmatrix(xc, xg)
            Ce = A[e] * E[e]
            ke += np.dot(np.dot(Be.T, Ce), Be) * Je * wg
        K[np.ix_(dofs, dofs)] += ke
    return K


def global_force(
    coords: NDArray, conn: NDArray, dload: NDArray, pload: NDArray
) -> NDArray:
    num_nodes = len(np.unique(conn))
    num_dofs = num_nodes * num_dofs_per_node
    F = np.zeros(num_dofs, dtype=float)
    for e, dofs in enumerate(conn):
        xc = coords[dofs]
        fe = np.zeros(2)
        for p in range(len(gauss_points)):
            xg = gauss_points[p]
            wg = gauss_weights[p]
            Je = jacobian(xc)
            Ne = shape(xg)
            fe += Je * wg * dload[e] * Ne
        F[np.ix_(dofs)] += fe
    F += pload
    return F


def apply_dirichlet_bc(
    K: NDArray, F: NDArray, dofs: NDArray, values: NDArray
) -> tuple[NDArray, NDArray]:
    """Apply Dirichlet (essential) boundary conditions to the global stiffness
    and global force.

    Parameters
    ----------
    K : ndarray
        The global stiffness (size: ndof x ndof)
    F : ndarray
        The global force (size: ndof)
    dofs : ndarray
        Essential boundary conditions are to be applied to dofs[i]
    values : ndarray
        values[i] is the value of the ith essential boundary condition

    """
    Kbc = np.array(K)
    Fbc = np.array(F)
    for i, dof in enumerate(dofs):
        Kbc[dof, :] = 0
        Kbc[dof, dof] = 1
        Fbc[dof] = values[i]
    return Kbc, Fbc


def jacobian(xc: NDArray) -> float:
    le = xc[1] - xc[0]
    return le / 2.0


def shape(xg: float) -> NDArray:
    """Returns 1D shape functions in natural coordinates"""
    N = np.array([(1.0 - xg) / 2.0, (1.0 + xg) / 2.0], dtype=float)
    return N


def grad(xg: float) -> NDArray:
    """Returns gradient of 1D shape functions in natural coordinates"""
    dN = np.array([-0.5, 0.5], dtype=float)
    return dN


def bmatrix(xc: NDArray, xg: float) -> NDArray:
    # B = dN/dx = dN/dq dq/dx = dN/dq 1/J = dN/dq 2/l
    b = grad(xg) / jacobian(xc)
    return b[np.newaxis]
