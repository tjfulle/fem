import numpy as np
from numpy.typing import NDArray


def norm(v: NDArray) -> float:
    return np.sqrt(np.dot(v, v))


def cost(a: NDArray, b: NDArray) -> float:
    """Compute the cosine of the angle between two vectors"""
    return np.dot(a, b) / norm(a) / norm(b)


def edge_normal(a: NDArray) -> NDArray:
    """Compute the normal to edge defined by the vector `a`"""
    a = np.asarray(a)
    dx, dy = a[1, :] - a[0, :]
    v = np.array([dy, -dx], dtype=float)
    return v / np.sqrt(np.dot(v, v))
