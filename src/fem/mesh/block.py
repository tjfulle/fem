from typing import Union

import numpy as np
from numpy.typing import NDArray

from ..element import Element


class ElementBlock:
    def __init__(
        self, *, name: str, element: Element, elements: Union[list[int], NDArray]
    ) -> None:
        self.element = element
        self.elements = np.asarray(elements)

    @property
    def dofs_per_node(self):
        return self.element.dofs_per_node

    def __contains__(self, element: int) -> bool:
        return element in self.elements
