from typing import Union

import numpy as np
from numpy.typing import NDArray


class Sideset:
    def __init__(self, *, name: str, sides: Union[NDArray, list[list[int]]]) -> None:
        self.name = name
        self.sides = np.asarray(sides)

    def __iter__(self):
        return iter(self.sides)


class Nodeset:
    def __init__(self, *, name: str, nodes: Union[NDArray, list[int]]) -> None:
        self.name = name
        self.nodes = np.asarray(nodes)

    def __iter__(self):
        return iter(self.nodes)


class ElementSet:
    def __init__(self, *, name: str, elements: Union[NDArray, list[int]]) -> None:
        self.name = name
        self.elements = np.asarray(elements)

    def __iter__(self):
        return iter(self.elements)
