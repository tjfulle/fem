from typing import Union

import exodusii
import numpy as np
from numpy.typing import NDArray

from ..element import Element
from ..io import aba
from ..io import vtk
from .block import ElementBlock
from .sets import ElementSet
from .sets import Nodeset
from .sets import Sideset


class Mesh:
    """Class for creating a finite element mesh.

    Parameters
    ----------
    nodes : list of list
        ``nodes[n]`` is a list describing the nth node where
        ``nodes[n][0]`` is the node label of the nth node and
        ``nodes[n][1:]`` are the nodal coordinates of the nth node
    elements : list of list
        ``elements[e]`` is a list describing the eth element where
        ``elements[e][0]`` is the element label of the eth element and
        ``elements[e][1:]`` are the nodes forming the eth element, the nodal
        labels must correpond to the node labels in ``nodes``
    file : str
        Name of a genesis or vtu file containing the mesh
    p : ndarray of float
        Array of nodal coordinates
    t : ndarray of int
        Array of element connectivity

    Notes
    -----
    The nodes/elements, file, and p/t arguments are mutually exclusive.

    If ``p`` and ``t`` are specified, node labels are assigned to nodes in the
    order they appear in the nodal coordinates array ``p`` and take values of
    ``0`` to ``n-1``, where ``n`` is the length of ``p``. The element
    connectivity array ``t`` must use this numbering scheme. This method is
    useful for creating meshes using triangulations created by the
    ``distmesh2d`` module.

    Examples
    --------

    In the following examples, a simple mesh is created of a unit square
    centered at the origin with node and element numbers as follows:

    .. image:: unitsquare1.png
       :scale: 35
       :align: center

    Method 1: specifying ``nodes`` and ``elements``:

    >>> nodes = [[10, -.5, -.5], [20, .5, -.5], [30, .5, .5], [40, -.5, .5]]
    >>> elements = [[100, 10, 20, 30, 40]]
    >>> mesh = Mesh(nodes=nodes, elements=elements)

    Method 2: specifying ``file``

    >>> mesh = Mesh(file="unitsquare.g")

    Method 3: specifying ``p`` and ``t``:

    >>> p = np.array([[-.5, -.5], [.5, -.5], [.5, .5], [-.5, .5]])
    >>> t = np.array([[0, 1, 2, 3]])
    >>> mesh = Mesh(p=p, t=t)

    """

    def __init__(self, *, nodes: list, elements: list) -> None:
        self.node_map: dict[int, int] = {}
        self.coords = np.zeros((len(nodes), 2))
        for iid, row in enumerate(nodes):
            id, x, y = row
            self.node_map[id] = iid
            self.coords[iid] = [x, y]
        self.elem_map: dict[int, int] = {}
        nnode = len(elements[0]) - 1
        self.conn = np.zeros((len(elements), nnode), dtype=int)
        for iid, row in enumerate(elements):
            id, *ni = row
            for n in ni:
                if n not in self.node_map:
                    raise ValueError(f"Node {n} of element {id} not defined")
            self.elem_map[id] = iid
            self.conn[iid] = [self.node_map[n] for n in ni]

        self.blocks: dict[str, ElementBlock] = {}
        self.nodesets: dict[str, Nodeset] = {}
        self.element_sets: dict[str, ElementSet] = {}
        self.sidesets: dict[str, Sideset] = {}

    @classmethod
    def read(cls, file: str) -> "Mesh":
        if file.endswith((".vtk", ".vtu")):
            nodes, elements, nsets, elsets, ssets = vtk.read_mesh(file)
            self = cls(nodes=nodes, elements=elements)
            if nsets:
                for ns_name, ns_nodes in nsets.items():
                    self.nodeset(ns_name, ns_nodes)
            if elsets:
                for es_name, es_elems in elsets.items():
                    self.element_set(es_name, es_elems)
            if ssets:
                for ss_name, ss_sides in ssets.items():
                    self.sideset(ss_name, ss_sides)

        elif file.endswith((".exo", ".g", ".gen")):
            exo = exodusii.exo_file(file, mode="r")
            xp = exo.get_coords()
            node_map = exo.get_node_id_map()
            nodes = [[id] + xp[iid].tolist() for (iid, id) in enumerate(node_map)]
            elements = []
            for bid in exo.get_element_block_ids():
                for row in exo.get_element_conn(bid):
                    elements.append([len(elements) + 1] + row.tolist())
            self = cls(nodes=nodes, elements=elements)
            if exo.num_node_sets():
                for id in exo.get_node_set_ids():
                    name = exo.get_node_set_name(id)
                    nodes = exo.get_node_set_nodes(id)
                    self.nodeset(name, nodes)
            if exo.num_side_sets():
                for id in exo.get_side_set_ids():
                    name = exo.get_side_set_name(id)
                    ss_sides = exo.get_side_set_sides(id)
                    ss_elems = exo.get_side_set_elems(id)
                    ss = list(zip(ss_elems, ss_sides))
                    self.sideset(name, ss)  # type: ignore

        elif file.endswith(".inp"):
            data = aba.read(file)
            nodes, elements, nsets, elsets, ssets = data
            self = cls(nodes=nodes, elements=elements)
            if nsets:
                for ns_name, ns_nodes in nsets.items():
                    self.nodeset(ns_name, ns_nodes)
            if elsets:
                for es_name, es_elems in elsets.items():
                    self.element_set(es_name, es_elems)
            if ssets:
                for ss_name, ss_sides in ssets.items():
                    self.sideset(ss_name, ss_sides)

        else:
            raise ValueError("Unknown file type")

        return self

    @classmethod
    def from_distmesh(cls, *, points: NDArray, triangulation: NDArray) -> "Mesh":
        nodes = []
        for i, p in enumerate(points, start=1):
            nodes.append([i, p[0], p[1]])
        elements = []
        for i, e in enumerate(triangulation, start=1):
            elements.append([i, e[0] + 1, e[1] + 1, e[2] + 1])
        return cls(nodes=nodes, elements=elements)

    @property
    def num_node(self):
        return self.coords.shape[0]

    @property
    def num_elem(self):
        return self.conn.shape[0]

    def element_block(
        self, name: str, *, element: Element, elements: Union[str, list[int], NDArray]
    ) -> ElementBlock:
        if name in self.blocks:
            raise ValueError(f"element block {name} already defined")
        iids: list[int] = []
        if isinstance(elements, str):
            if elements.lower() == "all":
                iids.extend(range(self.num_elem))
            else:
                raise ValueError(f"Unknown element designator {elements!r}")
        else:
            for id in elements:
                iid = self.elem_map.get(id)
                if iid is None:
                    raise ValueError(
                        f"attempting to put undefined element {id} in to block {name}"
                    )
                iids.append(iid)
        self.blocks[name] = ElementBlock(name=name, element=element, elements=iids)
        return self.blocks[name]

    def element_set(
        self, name: str, elements: Union[str, list[int], NDArray]
    ) -> ElementSet:
        if name in self.element_sets:
            raise ValueError(f"element set {name} already defined")
        iids: list[int] = []
        if isinstance(elements, str):
            if elements == "all":
                iids.extend(range(self.num_elem))
            else:
                raise ValueError(f"Unknown element designator {elements!r}")
        else:
            for id in elements:
                iid = self.elem_map.get(id)
                if iid is None:
                    raise ValueError(
                        f"attempting to put undefined element {id} in to set {name}"
                    )
                iids.append(iid)
        self.element_sets[name] = ElementSet(name=name, elements=iids)
        return self.element_sets[name]

    def nodeset(self, name: str, nodes: Union[str, list[int], NDArray]) -> Nodeset:
        if name in self.nodesets:
            raise ValueError(f"node set {name} already defined")
        iids: list[int] = []
        if isinstance(nodes, str):
            if nodes == "all":
                iids.extend(range(self.num_node))
            elif nodes in ("ilo", "ihi", "jlo", "jhi"):
                iids.extend(self.nodes_in_region(nodes))
            else:
                raise ValueError(f"Unknown node designator {nodes!r}")
        else:
            for id in nodes:
                iid = self.node_map.get(id)
                if iid is None:
                    raise ValueError(
                        f"attempting to put undefined node {id} in to set {name}"
                    )
                iids.append(iid)
        self.nodesets[name] = Nodeset(name=name, nodes=iids)
        return self.nodesets[name]

    def sideset(
        self, name: str, sides: Union[str, list[list[int]], NDArray]
    ) -> Sideset:
        if name in self.sidesets:
            raise ValueError(f"side set {name} already defined")
        sideset = []
        if isinstance(sides, str):
            if sides not in ("all", "ilo", "ihi", "jlo", "jhi"):
                raise ValueError(f"Unknown side designator {sides!r}")
            sideset.extend(self.sides_in_region(sides))
        else:
            for el, side in sides:
                if el not in self.elem_map:
                    raise ValueError(
                        f"attempting to put undefined element {id} in to set {name}"
                    )
                if 1 > side > 3:
                    raise ValueError(f"Invalid side ({el}, {side}) in {name}")
                sideset.append([self.elem_map[el], side - 1])
        self.sidesets[name] = Sideset(name=name, sides=sideset)
        return self.sidesets[name]

    def nodes_in_region(self, region: str, tol: float = 1.0e-6) -> NDArray:
        if region not in ("all", "ilo", "ihi", "jlo", "jhi"):
            raise ValueError(f"Invalid region {region!r}")
        if region == "all":
            return np.arange(self.num_node, dtype=int)
        axis, fun = {
            "ilo": (0, np.amin),
            "ihi": (0, np.amax),
            "jlo": (1, np.amin),
            "jhi": (1, np.amax),
        }[region]
        xpos = fun(self.coords[:, axis])
        ix = np.where(np.abs(self.coords[:, axis] - xpos) < tol)[0]
        return np.array(ix, dtype=int)

    def sides_in_region(self, region):
        if region not in ("all", "ilo", "ihi", "jlo", "jhi"):
            raise ValueError(f"Invalid 2D region {region}")
        nodes = self.nodes_in_region(region)
        sides = []
        for block in self.blocks.values():
            el = block.element
            for iel in block.elements:
                c = self.conn[iel]
                w = np.where(np.in1d(c, nodes))[0]
                if len(w) < 2:
                    continue
                w = tuple(sorted(w))
                for d, edge in enumerate(el.edges):
                    if tuple(sorted(edge)) == w:
                        # the internal node numbers match, this is the edge
                        sides.append((iel, d))
        return sides
