import os
from typing import Optional

import numpy as np
from numpy.typing import NDArray

from .mesh import Mesh

__all__ = [
    "unit_square_mesh",
    "rectilinear_mesh2d",
    "vtk_mesh",
    "abaqus_mesh",
    "genesis_mesh",
]


def rectilinear_mesh2d(
    nx: int = 1,
    ny: int = 1,
    lx: float = 1.0,
    ly: float = 1.0,
    shiftx: Optional[float] = None,
    shifty: Optional[float] = None,
    method: str = "default",
    order: int = 1,
) -> Mesh:
    """Generates a rectilinear 2D finite element mesh.

    Parameters
    ----------
    nx, ny : int
        number of elements in x and y directions, respectively
    lx, ly : float
        length of mesh bounding box in x and y directions, respectively

    Returns
    -------
    Mesh object

    """
    if nx < 1:
        raise ValueError("Requres at least 1 element in x")
    if ny < 1:
        raise ValueError("Requres at least 1 element in y")
    if lx < 0.0:
        raise ValueError("Requres positive length in x")
    if ly < 0.0:
        raise ValueError("Requres positive length in y")

    if order == 1:
        if method == "default":
            p, t = gen_quad4_pt_1(nx, ny, lx, ly, sx=shiftx, sy=shifty)
        elif method == "felippa":
            p, t = gen_quad4_pt_2(nx, ny, lx, ly, sx=shiftx, sy=shifty)
        else:
            raise ValueError("Wrong 2D meshing method for 1st order mesh")
    elif order == 2:
        if method not in ("default", "felippa"):
            raise ValueError("Wrong 2D meshing method for 2nd order mesh")
        p, t = gen_quad8_pt_2(nx, ny, lx, ly, sx=shiftx, sy=shifty)
    else:
        raise ValueError("Unsupported 2D meshing order")

    nodes, elements = gen_node_and_elem_tables(p, t)
    return Mesh(nodes=nodes, elements=elements)


def unit_square_mesh(
    nx: int = 1,
    ny: int = 1,
    shiftx: Optional[float] = None,
    shifty: Optional[float] = None,
    method: str = "default",
    order: int = 1,
) -> Mesh:
    """
    Generates a rectilinear 2D finite element mesh.

    Parameters
    ----------
    shape : tuple
        (nx, ny) where nx is the number elements in :math:`x` and ny
        number of element in :math:`y`.

    Returns
    -------
    Mesh object

    Notes
    -----
    This method calls the ``Mesh.rectilinear_mesh_2d`` class method and
    stores the returned mesh as the ``FEModel.mesh`` attribute.

    """
    return rectilinear_mesh2d(
        nx=nx,
        ny=ny,
        lx=1,
        ly=1,
        shiftx=shiftx,
        shifty=shifty,
        method=method,
        order=order,
    )


def gen_node_and_elem_tables(p: NDArray, t: NDArray) -> tuple[list, list]:
    nodes = [[n + 1] + list(x) for (n, x) in enumerate(p)]
    elements = [[e + 1] + list(c + 1) for (e, c) in enumerate(t)]
    return nodes, elements


def gen_quad4_pt_1(
    nx: int,
    ny: int,
    lx: float,
    ly: float,
    sx: Optional[float] = None,
    sy: Optional[float] = None,
) -> tuple[NDArray, NDArray]:
    sx, sy = sx or 0.0, sy or 0.0
    xpoints = np.linspace(0, lx, nx + 1) + sx
    ypoints = np.linspace(0, ly, ny + 1) + sy
    p = np.array([(x, y) for y in ypoints for x in xpoints])

    # Connectivity
    k = 0
    numele = nx * ny
    t = np.zeros((numele, 4), dtype=int)
    for elem_num in range(numele):
        ii = elem_num + k
        elem_nodes = [ii, ii + 1, ii + nx + 2, ii + nx + 1]
        t[elem_num, :] = elem_nodes
        if (elem_num + 1) % (nx) == 0:
            k += 1

    return p, t


def gen_quad4_pt_2(
    nx: int,
    ny: int,
    lx: float,
    ly: float,
    sx: Optional[float] = None,
    sy: Optional[float] = None,
) -> tuple[NDArray, NDArray]:
    """Generate a rectilinear mesh with quad4 elements"""

    # Bounding box
    sx, sy = sx or 0.0, sy or 0.0
    xc = np.array(
        [
            [0.0 + sx, 0.0 + sy],
            [lx + sx, 0.0 + sy],
            [lx + sx, ly + sy],
            [0.0 + sx, ly + sy],
        ]
    )

    def shape(x, y):
        a = np.array(
            [
                (1.0 - x) * (1.0 - y),
                (1.0 + x) * (1.0 - y),
                (1.0 + x) * (1.0 + y),
                (1.0 - x) * (1.0 + y),
            ]
        )
        return a / 4.0

    # Nodal coordinates
    numnod = (nx + 1) * (ny + 1)
    p = np.zeros((numnod, 2))
    k = 0
    for i in range(nx + 1):
        for j in range(ny + 1):
            xi = 2.0 * i / nx - 1.0
            eta = 2.0 * j / ny - 1.0
            N = shape(xi, eta)
            p[k] = (np.dot(N, xc[:, 0]), np.dot(N, xc[:, 1]))
            k += 1

    # Element connectivity
    numele = nx * ny
    t = np.zeros((numele, 4), dtype=int)
    k = 0
    for i in range(nx):
        for j in range(ny):
            c1 = (ny + 1) * i + j
            c2 = (c1 + 1) + ny
            t[k] = (c1, c2, c2 + 1, c1 + 1)
            k += 1

    return p, t


def gen_quad8_pt_2(
    nx: int,
    ny: int,
    lx: float,
    ly: float,
    sx: Optional[float] = None,
    sy: Optional[float] = None,
) -> tuple[NDArray, NDArray]:
    """Generate a rectilinear mesh with quad8 elements"""

    # Bounding box
    sx, sy = sx or 0.0, sy or 0.0
    xc = np.array([[sx, sy], [lx + sy, sy], [lx + sx, ly + sy], [sx, ly + sy]])

    def shape(x, y):
        a = np.array(
            [
                (1.0 - x) * (1.0 - y),
                (1.0 + x) * (1.0 - y),
                (1.0 + x) * (1.0 + y),
                (1.0 - x) * (1.0 + y),
            ]
        )
        return a / 4.0

    # Nodal coordinates
    numnod = (2 * nx + 1) * (2 * ny + 1) - nx * ny
    p = np.zeros((numnod, 2))
    k = 0
    for i in range(2 * nx + 1):
        for j in range(2 * ny + 1):
            if (i + 1) % 2 == 0 and (j + 1) % 2 == 0:
                continue
            xi = float(i - nx) / nx
            eta = float(j - ny) / ny
            N = shape(xi, eta)
            p[k] = (np.dot(N, xc[:, 0]), np.dot(N, xc[:, 1]))
            k += 1

    # Element connectivity
    numele = nx * ny
    t = np.zeros((numele, 8), dtype=int)
    k = 0
    for i in range(nx):
        for j in range(ny):
            c1 = (3 * ny + 2) * i + 2 * (j + 1) - 2
            c3 = c1 + 2 * ny - j + 1
            c2 = c3 + ny + j + 1
            t[k] = (c1, c2, c2 + 2, c1 + 2, c3, c2 + 1, c3 + 1, c1 + 1)
            k += 1

    return p, t


def genesis_mesh(filename: str) -> Mesh:
    """
    Generates a finite element mesh from a Genesis file.

    Parameters
    ----------
    filename : str
        The path to a valid Genesis file

    Returns
    -------
    Mesh object

    Notes
    -----
    This method calls ``mesh.Mesh`` with the ``filename`` keyword and
    stores the returned mesh as the ``FEModel.mesh`` attribute.

    """
    if not os.path.isfile(filename):
        raise ValueError("NO SUCH FILE {0!r}".format(filename))
    return Mesh.read(filename)


def abaqus_mesh(filename: str) -> Mesh:
    """
    Generates a finite element mesh from a Abaqus input file.

    Parameters
    ----------
    filename : str
        The path to a valid Genesis file

    Returns
    -------
    Mesh object

    Notes
    -----
    This method calls ``mesh.Mesh`` with the ``filename`` keyword and
    stores the returned mesh as the ``FEModel.mesh`` attribute.

    """
    if not os.path.isfile(filename):
        raise ValueError("NO SUCH FILE {0!r}".format(filename))
    return Mesh.read(filename)


def vtk_mesh(filename: str) -> Mesh:
    """
    Generates a finite element mesh from a vtk .vtu file.

    Parameters
    ----------
    filename : str
        The path to a valid .vtu file

    Returns
    -------
    Mesh object

    Notes
    -----
    This method calls ``mesh.Mesh`` with the ``filename`` keyword and
    stores the returned mesh as the ``FEModel.mesh`` attribute.

    """
    if not os.path.isfile(filename):
        raise ValueError("NO SUCH FILE {0!r}".format(filename))
    return Mesh.read(filename)
