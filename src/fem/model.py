from typing import Any
from typing import Callable
from typing import Optional
from typing import Union

import numpy as np
from numpy.typing import NDArray

from .element import Element
from .mesh import Mesh
from .mesh.block import ElementBlock
from .mesh.sets import ElementSet
from .mesh.sets import Nodeset
from .mesh.sets import Sideset


class Model:
    """A simple class representing a finite element model"""

    def __init__(self, name: str, *, mesh: Mesh) -> None:
        self.name = name
        self.mesh = mesh
        self.dimension = 2

        self.doftags = np.zeros((self.num_node, 7), dtype=int)
        self.dofvals = np.zeros((self.num_node, 7), dtype=float)
        self.dload: dict[int, Callable] = {}
        self.dsload: dict[int, dict[int, NDArray]] = {}

        self.K: Optional[NDArray] = None
        self.F: Optional[NDArray] = None
        self.d: Optional[NDArray] = None
        self.R: Optional[NDArray] = None
        self.stress: Optional[NDArray] = None
        self.strain: Optional[NDArray] = None
        self.elem_stress: Optional[NDArray] = None
        self.elem_strain: Optional[NDArray] = None

    @property
    def num_node(self):
        return self.mesh.num_node

    @property
    def num_elem(self):
        return self.mesh.num_elem

    @property
    def coords(self):
        return self.mesh.coords

    @property
    def conn(self):
        return self.mesh.conn

    @property
    def num_dof(self):
        return self.num_node * self.dofs_per_node

    @property
    def sidesets(self) -> dict[str, Sideset]:
        return self.mesh.sidesets

    @property
    def nodesets(self) -> dict[str, Nodeset]:
        return self.mesh.nodesets

    @property
    def element_sets(self) -> dict[str, ElementSet]:
        return self.mesh.element_sets

    @property
    def blocks(self) -> dict[str, ElementBlock]:
        return self.mesh.blocks

    @property
    def dofs_per_node(self):
        for block in self.blocks.values():
            return block.dofs_per_node

    @property
    def force(self) -> NDArray:
        if self.F is None:
            raise ValueError("Model has not yet been solved")
        return self.F

    @property
    def stiffness(self) -> NDArray:
        if self.K is None:
            raise ValueError("Model has not yet been solved")
        return self.K

    @property
    def reaction(self) -> NDArray:
        if self.R is None:
            raise ValueError("Model has not yet been solved")
        return self.R

    @property
    def dofs(self) -> NDArray:
        if self.u is None:
            raise ValueError("Model has not yet been solved")
        return self.u

    def find(self, *, what: Any, iid: int) -> Any:
        if what == "element":
            for block in self.blocks.values():
                if iid in block.elements:
                    return block.element
            return None
        raise ValueError(what)

    def element_block(
        self, name: str, *, element: Element, elements: Union[str, list[int], NDArray]
    ) -> None:
        if tuple(element.node_freedom_table) != (1, 1, 0, 0, 0, 0, 0):
            raise TypeError("expected element with only temperature degree of freedom")
        self.mesh.element_block(name, element=element, elements=elements)

    def element_set(
        self, name: str, *, elements: Union[str, list[int], NDArray]
    ) -> None:
        self.mesh.element_set(name=name, elements=elements)

    def nodeset(self, name: str, *, nodes: Union[str, list[int], NDArray]) -> None:
        self.mesh.nodeset(name=name, nodes=nodes)

    def sideset(
        self, name: str, *, sides: Union[str, list[list[int]], NDArray]
    ) -> None:
        self.mesh.sideset(name=name, sides=sides)

    def displacement(
        self,
        nodes_or_nodeset: Union[str, NDArray],
        *,
        amplitude: float,
        dofs: Union[int, list[int]],
    ) -> None:
        if isinstance(dofs, int):
            dofs = [dofs]
        for dof in dofs:
            if dof not in (1, 2):
                raise ValueError(f"Expected dof to be 1 or 2 but got {dof}")
        if isinstance(nodes_or_nodeset, str):
            if nodes_or_nodeset not in self.nodesets:
                raise ValueError(f"{nodes_or_nodeset} is not a valid node set")
        else:
            # create an internal nodeset
            i, fmt = 0, "_Nodeset-{0}"
            if isinstance(nodes_or_nodeset, int):
                nodes_or_nodeset = [nodes_or_nodeset]
            nodes_or_nodeset = [self.mesh.node_map[id] for id in nodes_or_nodeset]
            while True:
                name = fmt.format(i)
                if name not in self.nodesets:
                    self.nodesets[name] = Nodeset(name=name, nodes=nodes_or_nodeset)
                    nodes_or_nodeset = name
                    break
                i += 1
        for iid in self.nodesets[nodes_or_nodeset]:
            for dof in dofs:
                self.doftags[iid, dof - 1] = 1
                self.dofvals[iid, dof - 1] = amplitude

    def gravity(
        self, elset: Union[str, NDArray], *, amplitude: float, components: list[float]
    ) -> None:
        b = amplitude * np.asarray(components)
        self.distributed_load(elset, amplitude=lambda _: b)

    def distributed_load(
        self, elset: Union[str, NDArray], *, amplitude: Callable
    ) -> None:
        if isinstance(elset, str):
            if elset not in self.element_sets:
                raise ValueError(f"{elset} is not a valid element set")
        else:
            # create an internal element
            i, fmt = 0, "_Elset-{0}"
            while True:
                name = fmt.format(i)
                if name not in self.element_sets:
                    self.element_sets[name] = ElementSet(name=name, elements=elset)
                    elset = name
                    break
                i += 1
        for iid in self.element_sets[elset]:
            self.dload[iid] = amplitude

    def surface_load(self, sideset: str, *, q: list[float]) -> None:
        if sideset not in self.sidesets:
            raise ValueError(f"{sideset} is not a valid side set")
        ss = self.sidesets[sideset]
        qa = np.asarray(q[:2])
        for iel, side in ss:
            self.dsload.setdefault(iel, {})[side] = qa

    def assemble_stiffness(self) -> NDArray:
        """Assemble the global stiffness matrix for a single dof/node problem

        Returns
        -------
        stiff : ndarray
            Global stiffness matrix stored as a full (nd*n, nd*n) symmetric matrix,
            where nd is the number of degrees of freedom per node and n the total
            number of nodes.

        """
        K = np.zeros((self.num_dof, self.num_dof), dtype=float)
        for block in self.blocks.values():
            ndof = block.dofs_per_node
            for iel in block.elements:
                nodes = self.mesh.conn[iel]
                dofs = [n * ndof + k for n in nodes for k in range(ndof)]
                xc = self.mesh.coords[nodes]
                ke = block.element.stiffness(xc)
                K[np.ix_(dofs, dofs)] += ke
        return K

    def assemble_force(self) -> NDArray:
        F = np.zeros(self.num_dof, dtype=float)
        for block in self.blocks.values():
            ndof = block.dofs_per_node
            for iel in block.elements:
                nodes = self.mesh.conn[iel]
                dofs = [n * ndof + k for n in nodes for k in range(ndof)]
                xc = self.mesh.coords[nodes]
                fe = block.element.force(
                    xc, dload=self.dload.get(iel), dsload=self.dsload.get(iel)
                )
                F[np.ix_(dofs)] += fe
        return F

    def apply_dirichlet_bcs(self, K: NDArray, F: NDArray) -> tuple[NDArray, NDArray]:
        """Apply Dirichlet (essential) boundary conditions to the global stiffness
        and global force.

        """
        Kbc = np.array(K)
        Fbc = np.array(F)

        # extract only the active dofs for the displacement dofs
        doftags = self.doftags[:, [0, 1]]
        dofvals = self.dofvals[:, [0, 1]]
        for i, tags in enumerate(doftags):
            for j, tag in enumerate(tags):
                # i is the node number, j is the local dof
                if tag == 1:
                    dof = i * self.dofs_per_node + j
                    Kbc[dof, :] = 0
                    Kbc[dof, dof] = 1
                    Fbc[dof] = dofvals[i, j]
        return Kbc, Fbc

    def solve(self) -> None:
        self.K = self.assemble_stiffness()
        self.F = self.assemble_force()
        Kbc, Fbc = self.apply_dirichlet_bcs(self.K, self.F)
        self.d = np.linalg.solve(Kbc, Fbc)
        self.R = np.dot(self.K, self.d) - self.F

    def post(self) -> None:
        # compute nodal values of strain and stress by averaging element
        # stresses to the nodes
        counter = np.zeros(self.num_node, dtype=int)
        self.stress = np.zeros((self.num_node, 3), dtype=float)
        self.strain = np.zeros((self.num_node, 3), dtype=float)
        self.elem_stress = np.zeros((self.num_elem, 3), dtype=float)
        self.elem_strain = np.zeros((self.num_elem, 3), dtype=float)
        self.mises = np.zeros(self.num_node, dtype=float)
        u = self.d.reshape((self.num_node, -1))
        for block in self.blocks.values():
            ndof = block.dofs_per_node
            for iel in block.elements:
                nodes = self.mesh.conn[iel]
                dofs = [n * ndof + k for n in nodes for k in range(ndof)]
                xc = self.mesh.coords[nodes] + u[nodes]
                elem_strain = np.zeros((len(block.element.gauss_points), 3))
                elem_stress = np.zeros((len(block.element.gauss_points), 3))
                for p, xg in enumerate(block.element.gauss_points):
                    Be = block.element.bmatrix(xc, xg)
                    elem_strain[p] = np.dot(Be, self.d[dofs])
                    D = block.element.material.stiffness(ndi=2, nshr=1)
                    elem_stress[p] = np.dot(D, elem_strain[p])
                elem_strain = np.mean(elem_strain, axis=0)
                elem_stress = np.mean(elem_stress, axis=0)
                for node in nodes:
                    count = counter[node]
                    if not count:
                        e = elem_strain
                        s = elem_stress
                    else:
                        e = (count * self.strain[node, :] + elem_strain) / (count + 1)
                        s = (count * self.stress[node, :] + elem_stress) / (count + 1)
                    self.strain[node, :] = e
                    self.stress[node, :] = s
                    self.elem_strain[iel, :] = elem_strain
                    self.elem_stress[iel, :] = elem_stress
                    vm = np.sqrt(
                        0.5
                        * ((s[0] - s[1]) ** 2 + s[0] ** 2 + s[1] ** 2 + 6 * s[2] ** 2)
                    )
                    self.mises[node] = vm
                    counter[node] += 1
        un = [np.sqrt(np.sum(_**2)) for _ in u]
        print(f"Max displacemnt = {np.amax(un)}")
        print(f"Max Mises stress = {np.amax(self.mises)}")

    def triplot(self, show: bool = False, save: Optional[str] = None) -> None:
        import matplotlib.pyplot as plt
        import matplotlib.tri as mtri

        u = self.d.reshape((self.num_node, -1))
        scale = 0.25 / np.amax(np.abs(u))
        xc = self.mesh.coords + scale * u
        triang = mtri.Triangulation(xc[:, 0], xc[:, 1], self.mesh.conn)
        un = [np.linalg.norm(_) for _ in u]  # noqa: F841
        normalize = lambda x: (x - np.amin(x)) / (np.amax(x) - np.amin(x))
        normalize = lambda x: x
        s = 100 * normalize(self.stress[:, 0])
        s = 100 * normalize(self.mises)
        plt.tricontourf(triang, s, cmap="turbo", levels=540)
        plt.triplot(triang)
        # plt.quiver()
        plt.gca().set_aspect("equal")
        if show:
            plt.show()
        if save is not None:
            plt.savefig(save)
        return
