# Finite element assembly

## Definition

The process of scattering the element response into the matrices defining the  global state.

## Uniform degree of freedom

Each element in the finite element mesh has the same degree of freedom signature.

## Algorithm

### Global stiffness

```
FOREACH ELEMENT IN MESH
  COMPUTE THE ELEMENT STIFFNESS KE
  SCATTER KE INTO GLOBAL STIFFNESS KG
END FOREACH
```

To scatter `KE` into `KG` we need to know:

- mapping of local degree of freedom index to global degree of freedom index
- mapping of element and local node number to local degree of freedom

Both mappings can be provided by an "element freedom table".

## Element freedom table

Element freedom table is a table that is (number of elements) x (max number of degrees of freedom per element).  `eft[e, n]` is the global degree of freedom of the `n`th local degree of freedom of element `e`

For example, consider the following mesh

```console
5     3     4
o-----o-----o
|  0  |  1  |
o-----o-----o
0     2     1
```

The element freedom table is

|el/local dof| 0 | 1 | 2 | 3 |
|---|---|---|---|---|
| 0 | 0 | 2 | 3 | 5 |
| 1 | 2 | 1 | 4 | 3 |

To generate on the element freedom table for an element in uniform mesh:

```python
for element in mesh:
    for node in element:
        for local_dof in range(dofs_per_node):
            global_dof = node * dofs_per_node + local_dof
```
