import numpy as np
from fem import bar


def tapered_bar(
    *,
    L: float,
    E: float,
    A0: float,
    Al: float,
    g: float,
    rho: float,
    Sl: float,
    N: int,
):
    xc, conn = bar.mesh(L, num_ele=N)

    xe = [(xc[i] + xc[i + 1]) / 2.0 for i in range(N)]
    Ae = np.array([A0 + (Al - A0) * x / L for x in xe])
    Ee = np.ones(N) * E

    K = bar.global_stiffness(xc, conn, Ae, Ee)

    dload = -rho * g * Ae
    P = np.zeros(N + 1)
    P[-1] = -Sl * Al
    F = bar.global_force(xc, conn, dload, P)

    Kbc, Fbc = bar.apply_dirichlet_bc(K, F, [0], [0.0])

    u = np.linalg.solve(Kbc, Fbc)
    R = np.dot(K, u) - F
    print(f"u={u[-1]*1e3} [mm]")
    print(f"R={R[-1]}")


if __name__ == "__main__":
    g = 9.81
    rho = 7800
    L, N = 3.0, 2
    A0, AL = 0.5, 0.3
    sigma = 2e6
    tapered_bar(
        L=3.0,
        N=200,
        A0=0.5,
        Al=0.3,
        E=70.0e9,
        g=9.81,
        rho=7800.0,
        Sl=1.0e6,
    )
