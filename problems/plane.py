import argparse

import numpy as np
from fem.element import C2D3S
from fem.material import LinearElastic
from fem.mesh import Mesh
from fem.model import Model


def plate_with_hole(problem="fine", show_plot=False):
    # Two mesh files: PlateWithHoleTria3.g and PlateWithHoleTria3Fine.g
    files = {"coarse": "PlateWithHoleTria3.g", "fine": "PlateWithHoleTria3Fine.g"}
    file = files[problem]
    mesh = Mesh.read(file)
    model = Model("Heat", mesh=mesh)

    material = LinearElastic(E=30e9, nu=0.3, rho=2400.0)
    element = C2D3S(material=material)
    model.element_block("Block-1", elements="all", element=element)

    # Node sets are defined in the mesh file
    # other node sets are 'TopLeft', 'BottomLeft', 'BottomRight',
    # 'Bottom', 'Top', 'TopRight',
    model.displacement("Top", amplitude=0, dofs=[1, 2])
    model.sideset("Sideset-1", sides="jlo")
    model.surface_load("Sideset-1", q=[400e3, -300e3])
    model.element_set("Elset-1", elements="all")
    model.gravity("Elset-1", amplitude=9.81 * material.density, components=[0, -1])

    model.solve()
    model.post()

    if show_plot:
        model.triplot(show=True)


def main():
    p = argparse.ArgumentParser()
    p.add_argument(
        "problem",
        nargs="?",
        default="plate-with-hole",
        choices=("ex", "example", "plate-with-hole"),
    )
    p.add_argument("-p", "--plot", action="store_true", default=False)
    args = p.parse_args()

    plate_with_hole(show_plot=args.plot)


if __name__ == "__main__":
    main()
