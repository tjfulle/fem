from fem.material import MaterialDriver
from fem.material import J2Plastic


def plastic():
    driver = MaterialDriver()
    driver.material = J2Plastic(E=7.875e+10, nu=.3125, k=60e6)
    driver.add_deformation_step(1.0, [1.005, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0])
    driver.add_deformation_step(2.0, [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0])
    state = driver.run(delta_time=0.02)
    driver.plot(state)


if __name__ == "__main__":
    plastic()
