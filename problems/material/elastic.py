from fem.material import MaterialDriver
from fem.material import LinearElastic


def elastic():
    driver = MaterialDriver()
    driver.material = LinearElastic(E=30e6, nu=.3)
    driver.add_deformation_step(1.0, [1.00, 0.0, 0.0, 0.0, 1.5, 0.0, 0.0, 0.0, 1.0])
    driver.add_deformation_step(2.0, [1.00, 0.0, 0.0, 0.5, 1.5, 0.0, 0.0, 0.0, 1.0])
    driver.add_deformation_step(3.0, [1.00, 0.0, 0.0, 0.5, 0.5, 0.0, 0.0, 0.0, 1.0])
    driver.add_deformation_step(4.0, [1.00, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 1.0])
    driver.add_deformation_step(5.0, [1.00, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0])
    state = driver.run(delta_time=0.2)
    driver.plot(state)


if __name__ == "__main__":
    elastic()
