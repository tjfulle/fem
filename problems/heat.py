import argparse

import numpy as np
from fem.element import DC2D3
from fem.heat import Model
from fem.mesh import Mesh


def class_example_1(show_plot=False):
    # Geometry and loads from example in lecture notes from Jan 22, 2024
    nodes = [[1, 0.0, 0.0], [2, 0.0, 3.0], [3, 4.0, 3.0], [4, 4.0, 0.0]]
    elements = [[1, 1, 3, 2], [2, 1, 4, 3]]

    mesh = Mesh(nodes=nodes, elements=elements)
    model = Model("Heat", mesh=mesh)

    model.element_block("Block-1", elements=[1, 2], element=DC2D3(k=12))

    model.sideset("Sideset-1", sides="ilo")
    model.surface_flux("Sideset-1", q=[100, 0])

    model.sideset("Sideset-2", sides="ihi")
    model.surface_film("Sideset-2", h=25, Too=10)

    model.element_set("Elset-1", elements="all")
    model.heat_source("Elset-1", source=lambda x: 500 * (x[0] ** 2 + x[1]))

    model.nodeset("Nodeset-1", nodes="jlo")
    model.temperature("Nodeset-1", amplitude=50)

    model.solve()

    # verify that the model solution matches the analytic solution
    u_analytic = [50, 548.235, 554.973, 50]
    assert np.allclose(model.dofs, u_analytic)

    # verify that the reaction matches the analytic solution
    r_analytic = [-12830.3, 0, 0, -6533.18]
    assert np.allclose(model.reaction, r_analytic)

    if show_plot:
        model.triplot(show=True)


def plate_with_hole(problem="fine", show_plot=False):
    # Two mesh files: PlateWithHoleTria3.g and PlateWithHoleTria3Fine.g
    files = {"coarse": "PlateWithHoleTria3.g", "fine": "PlateWithHoleTria3Fine.g"}
    file = files[problem]
    mesh = Mesh.read(file)
    model = Model("Heat", mesh=mesh)

    model.element_block("Block-1", elements="all", element=DC2D3(k=12))

    # Node sets are defined in the mesh file
    # other node sets are 'TopLeft', 'BottomLeft', 'BottomRight',
    # 'Bottom', 'Top', 'TopRight',
    model.temperature("LeftHandSide", amplitude=200)
    model.temperature("RightHandSide", amplitude=50)

    model.sideset("Sideset-1", sides="jlo")
    model.surface_flux("Sideset-1", q=[0, 2000])

    model.sideset("Sideset-2", sides="jhi")
    model.surface_film("Sideset-2", h=250, Too=25)

    model.element_set("Elset-1", elements="all")
    model.heat_source("Elset-1", source=lambda x: 1000 / np.sqrt(x[0] ** 2 + x[1] ** 2))

    model.solve()

    if show_plot:
        import matplotlib.pyplot as plt

        ns = model.nodesets["LeftHandSide"]
        dofs = ns.nodes
        yp = model.coords[dofs, 1]
        rx = model.reaction[dofs]
        plt.plot(yp, rx, ".")
        plt.show()

        plt.clf()
        ns = model.nodesets["RightHandSide"]
        dofs = ns.nodes
        yp = model.coords[dofs, 1]
        rx = model.reaction[dofs]
        plt.plot(yp, rx, ".")
        plt.show()

    if show_plot:
        model.triplot(show=True)


def main():
    p = argparse.ArgumentParser()
    p.add_argument(
        "problem",
        nargs="?",
        default="plate-with-hole",
        choices=("ex", "example", "plate-with-hole"),
    )
    p.add_argument("-p", "--plot", action="store_true", default=False)
    args = p.parse_args()

    if args.problem.startswith("ex"):
        class_example_1(show_plot=args.plot)
    else:
        plate_with_hole(show_plot=args.plot)


if __name__ == "__main__":
    main()
