import numpy as np
from fem.element import C2D3S
from fem.material import LinearElastic


def test_2d_triangle():
    coords = np.array([[0, 0], [3, 1], [2, 2]], dtype=float)
    material = LinearElastic(E=60.0, nu=0.25, rho=1)
    element = C2D3S(material=material, t=1.0)
    ke = element.stiffness(coords)
    expected = np.array(
        [
            [11, 5, -10, -2, -1, -3],
            [5, 11, 2, 10, -7, -21],
            [-10, 2, 44, -20, -34, 18],
            [-2, 10, -20, 44, 22, -54],
            [-1, -7, -34, 22, 35, -15],
            [-3, -21, 18, -54, -15, 75],
        ],
        dtype=float,
    )
    assert np.allclose(ke, expected)
