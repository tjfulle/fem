import fem
import fem.element
import fem.io.plot
import fem.material
import fem.mesh
import fem.model


def test_uniaxial_stress():
    mesh = fem.mesh.rectilinear_mesh2d(nx=60, ny=30, lx=2, ly=1)
    material = fem.material.LinearElastic(E=10, nu=0.0, rho=1.0)
    element = fem.element.C2D4S(material=material)
    model = fem.model.Model("Plane", mesh=mesh)
    model.element_block("All", element=element, elements="All")
    model.nodeset("Nodeset-1", nodes="ilo")
    model.displacement("Nodeset-1", dofs=[1], amplitude=0.0)
    model.sideset("Sideset-1", sides="ihi")
    model.surface_load("Sideset-1", q=[100, 0])
    model.solve()
    model.post()
    s = model.elem_stress[:, 0]
    fem.io.plot.plot2d(model, show=True, scale=100, colorby=s)


test_uniaxial_stress()
