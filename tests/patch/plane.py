import fem.mesh
import fem.model
from fem.element import C2D3S
from fem.material import LinearElastic
from numpy import allclose


def test_3_node_plane_stress():
    nodes = [
        [0, 0.0, 0.0],
        [1, 0.24, 0.0],
        [22, 0.24, 0.12],
        [3, 0.0, 0.12],
        [14, 0.04, 0.02],
        [5, 0.18, 0.03],
        [6, 0.16, 0.08],
        [99, 0.08, 0.08],
    ]
    elements = [
        [1, 0, 1, 5],
        [2, 0, 5, 14],
        [3, 3, 0, 14],
        [4, 3, 14, 99],
        [5, 1, 22, 6],
        [6, 1, 6, 5],
        [7, 22, 3, 99],
        [8, 22, 99, 6],
        [9, 14, 5, 6],
        [10, 14, 6, 99],
    ]
    mesh = fem.mesh.Mesh(nodes=nodes, elements=elements)
    model = fem.model.Model("Plane", mesh=mesh)
    material = LinearElastic(E=1e6, nu=0.25, rho=1.0)
    element = C2D3S(material=material)
    model.element_block("Block-1", elements="all", element=element)

    model.displacement([0], amplitude=0.0, dofs=[1, 2])
    model.displacement([1], dofs=1, amplitude=2.4e-4)
    model.displacement([1], dofs=2, amplitude=1.2e-4)
    model.displacement([22], dofs=1, amplitude=3.0e-4)
    model.displacement([22], dofs=2, amplitude=2.4e-4)
    model.displacement([3], dofs=1, amplitude=6.0e-5)
    model.displacement([3], dofs=2, amplitude=1.2e-4)
    model.solve()
    model.post()
    s = model.stress
    assert allclose(s[:, :2], 1331.668)
    assert allclose(s[:, 2], 399.2)


if __name__ == "__main__":
    test_3_node_plane_stress()
